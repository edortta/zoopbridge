<?php

require_once dirname(__FILE__) . "/MarketplaceBase.php";

class ObjetosBaseZoop extends MarketplaceBase
{

/*
 * Verifica que os dados passados nos parâmetros
 * sejam válidos para gerar/modificar o cadastro de um vendedor
 */
    private function _verificarValidadeCadastroVendedor(
        $cpf,
        $nome, $sobrenome,
        $apelido,
        $descricao,
        $data_nascimento,
        $endereco_logradouro,
        $endereco_numero,
        $endereco_complemento,
        $endereco_cep,
        $endereco_bairro,
        $endereco_cidade,
        $endereco_uf,
        $endereco_pais_iso3166,
        $email,
        $telefone,
        $website,
        $atividade_mcc) {
        $ret = false;
        if (($website === null) || ($website === '') || ($this->isValidURL($website))) {
            if ($this->isValidEmail($email)) {
                if ($this->isValidDate($data_nascimento, 'Y-m-d')) {
                    if ($this->CPFCorreto($cpf)) {
                        $ret = true;
                    } else {
                        $ret = $this->triggerError('CPF inválido');
                    }
                } else {
                    $ret = $this->triggerError('Data de nascimento inválida');
                }
            } else {
                $ret = $this->triggerError('E-mail inválido');
            }
        } else {
            $ret = $this->triggerError('Website inválido');
        }

        return $ret;
    }

    /*
     * realiza o cadastro de uma pessoa física tanto vendedor (sellers)
     * como comprador (buyers)
     */
    private function _cadastrarCPF(
        $cTipoPessoa,
        $cpf,
        $nome, $sobrenome,
        $apelido,
        $descricao,
        $data_nascimento,
        $endereco_logradouro,
        $endereco_numero,
        $endereco_complemento,
        $endereco_cep,
        $endereco_bairro,
        $endereco_cidade,
        $endereco_uf,
        $endereco_pais_iso3166,
        $email,
        $telefone,
        $delinquent = null,
        $default_debit = null,
        $default_credit = null,
        $website = null,
        $atividade_mcc = null,
        $account_balance = null,
        $current_balance = null,
        $fiscal_responsibility = null,
        $metadata = '',
        $created_at = null,
        $updated_at = null) {
        $ret = $this->_verificarValidadeCadastroVendedor($cpf,
            $nome, $sobrenome,
            $apelido,
            $descricao,
            $data_nascimento,
            $endereco_logradouro,
            $endereco_numero,
            $endereco_complemento,
            $endereco_cep,
            $endereco_bairro,
            $endereco_cidade,
            $endereco_uf,
            $endereco_pais_iso3166,
            $email,
            $telefone,
            $website,
            $atividade_mcc);

        if ($ret === true) {
            $ret = $this->privateRequest(
                'POST',
                "/$cTipoPessoa/individuals",
                [
                    'fiscal_responsibility' => null,
                    'first_name'            => $nome,
                    'last_name'             => $sobrenome,
                    'email'                 => $email,
                    'website'               => $website,
                    'phone_number'          => $telefone,
                    'taxpayer_id'           => $cpf,
                    'birthdate'             => $data_nascimento,
                    'statement_descriptor'  => $apelido,
                    'description'           => $descricao,
                    'address'               => [
                        'line1'        => $endereco_logradouro,
                        'line2'        => $endereco_numero,
                        'line3'        => $endereco_complemento,
                        'neighborhood' => $endereco_bairro,
                        'city'         => $endereco_cidade,
                        'state'        => $endereco_uf,
                        'postal_code'  => $endereco_cep,
                        'country_code' => $endereco_pais_iso3166,
                    ],
                    'account_balance'       => $account_balance,
                    'current_balance'       => $current_balance,
                    'fiscal_responsibility' => $fiscal_responsibility,

                    'mmc'                   => $atividade_mcc,
                    'metadata'              => $metadata,
                    'created_at'            => $created_at,
                    'updated_at'            => $updated_at,
                ]
            );
        }

        return $ret;
    }

    /*
     * Buscando de vendedor por CPF/CNPJ
     * @author Esteban D.Dortta
     * @todo FALTA testar com CNPJ
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sellers/search
     * @param string $marketplace_id
     */
    private function _recuperarPessoaPorDocumento($cTipoPessoa, $documento)
    {
        $ePessoaFisica   = $this->CPFCorreto($documento);
        $ePessoaJuridica = $this->CNPJCorreto($documento);
        if ($ePessoaJuridica || $ePessoaFisica) {
            if ($ePessoaFisica) {
                $params = ['taxpayer_id' => $documento];
            } else {
                $params = ['ein' => $documento];
            }

            $ret = $this->privateRequest(
                'GET',
                "/$cTipoPessoa/search",
                $params
            );
        } else {
            $ret = $this->triggerError('Documento inválido');
        }
        return $ret;
    }

    private function _recuperarDetalhesPessoa($cTipoPessoa, $id)
    {
        $ret = $this->privateRequest(
            'GET',
            "/$cTipoPessoa/$id"
        );

        return $ret;
    }

    private function __addArray($cName, $aValues)
    {
        $ret = null;
        if (is_array($aValues)) {
            if ($aValues['amount'] > 0) {
                $ret = [$cName => $aValues];
            }
        }
        return $ret;
    }

    private function _criarTransacaoSemToken(
        $customer,
        $on_behalf_of,

        $reference_id,
        $amount,
        $description,

        $payment_method_expiration_date,
        $payment_method_payment_limit_date,
        $payment_method_late_fee,
        $payment_method_interest,
        $payment_method_discount_limit_date,
        $payment_method_discount,
        $payment_method_body_instructions,
        $payment_method_logo,

        $cardHolderName,
        $cardExpirationMonth,
        $cardExpirationYear,
        $cardNumber,
        $cardSecurityCode,

        $capture = true,
        $payment_type = 'boleto',
        $currency = 'BRL') {
        $payment_type = mb_strtolower(trim($payment_type));
        $kValueName   = null;
        $aValue       = null;

        switch ($payment_type) {
            case 'boleto':
                if ($payment_method_interest > 0) {
                    $aDiscount = [
                        'discount' => [
                            'mode'       => 'FIXED',
                            'limit_date' => $payment_method_discount_limit_date,
                            'amount'     => $payment_method_interest],
                    ];
                } else {
                    $aDiscount = null;
                }

                $kValueName = "payment_method";

                $aValue = [
                    'expiration_date'      => $payment_method_expiration_date,
                    'payment_limit_date'   => $payment_method_payment_limit_date,
                    'body_instructions'    => [$payment_method_body_instructions],
                    'billing_instructions' => [
                        $this->__addArray('late_fee', [
                            'mode'   => 'FIXED',
                            'amount' => $payment_method_late_fee]),
                        $this->__addArray('interest', [
                            'mode'   => 'DAILY_AMOUNT',
                            'amount' => $payment_method_interest]),
                        $aDiscount,
                    ]];
                break;
            case 'credit':
                $kValueName = "card";
                $aValue     = [
                    "holder_name"      => $cardHolderName,
                    "expiration_month" => $cardExpirationMonth,
                    "expiration_year"  => $cardExpirationYear,
                    "card_number"      => $cardNumber,
                    "security_code"    => $cardSecurityCode,
                ];
                break;

            default:
                $this->throwException(102, $payment_type);

                break;
        }

        $ret = $this->privateRequest(
            'POST',
            '/transactions',
            [
                'amount'       => $amount,
                'currency'     => $currency,
                'description'  => $description,
                'reference_id' => $reference_id,
                'on_behalf_of' => $on_behalf_of,
                'customer'     => $customer,
                'payment_type' => $payment_type,
                // 'capture'      => $capture,
                'logo'         => $payment_method_logo,
                $kValueName    => $aValue,
            ],
            ["Content-Type: text/plain"]
        );

        return $ret;
    }

    public function cadastrarVendedorCPF(
        $cpf,
        $nome, $sobrenome,
        $apelido,
        $descricao,
        $data_nascimento,
        $endereco_logradouro,
        $endereco_numero,
        $endereco_complemento,
        $endereco_cep,
        $endereco_bairro,
        $endereco_cidade,
        $endereco_uf,
        $endereco_pais_iso3166,
        $email,
        $telefone,
        $delinquent = false,
        $default_debit = null,
        $default_credit = null,
        $website = null,
        $atividade_mcc = null,
        $account_balance = null,
        $current_balance = null,
        $fiscal_responsibility = null,
        $metadata = '',
        $created_at = null,
        $updated_at = null) {
        $ret = $this->_cadastrarCPF(
            "sellers",
            $cpf,
            $nome, $sobrenome,
            $apelido,
            $descricao,
            $data_nascimento,
            $endereco_logradouro,
            $endereco_numero,
            $endereco_complemento,
            $endereco_cep,
            $endereco_bairro,
            $endereco_cidade,
            $endereco_uf,
            $endereco_pais_iso3166,
            $email,
            $telefone,
            $delinquent,
            $default_debit,
            $default_credit,
            $website,
            $atividade_mcc,
            $account_balance,
            $current_balance,
            $fiscal_responsibility,
            $metadata,
            $created_at,
            $updated_at
        );

        return $ret;
    }

    /*
     * Alterar um vendedor do tipo indivíduo
     * @author Esteban D.Dortta
     * @todo FALTA testar
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sellers/individuals/:id
     * @param string $id
     */
    public function alterarUmVendedorDoTipoIndividuo(
        $id,
        $cpf,
        $nome, $sobrenome,
        $apelido,
        $descricao,
        $data_nascimento,
        $endereco_logradouro,
        $endereco_numero,
        $endereco_complemento,
        $endereco_cep,
        $endereco_bairro,
        $endereco_cidade,
        $endereco_uf,
        $endereco_pais_iso3166,
        $email,
        $telefone,
        $website,
        $atividade_mcc) {
        $podeCadastrar = $this->_verificarValidadeCadastroVendedor($cpf,
            $nome, $sobrenome,
            $apelido,
            $descricao,
            $data_nascimento,
            $endereco_logradouro,
            $endereco_numero,
            $endereco_complemento,
            $endereco_cep,
            $endereco_bairro,
            $endereco_cidade,
            $endereco_uf,
            $endereco_pais_iso3166,
            $email,
            $telefone,
            $website,
            $atividade_mcc);

        if ($podeCadastrar === true) {
            $ret = $this->privateRequest(
                'PUT',
                "/sellers/individuals/$id",
                [
                    'fiscal_responsibility' => null,
                    'first_name'            => $nome,
                    'last_name'             => $sobrenome,
                    'email'                 => $email,
                    'website'               => $website,
                    'phone_number'          => $telefone,
                    'taxpayer_id'           => $cpf,
                    'birthdate'             => $data_nascimento,
                    'statement_descriptor'  => $apelido,
                    'description'           => $descricao,
                    'address'               => [
                        'line1'        => $endereco_logradouro,
                        'line2'        => $endereco_numero,
                        'line3'        => $endereco_complemento,
                        'neighborhood' => $endereco_bairro,
                        'city'         => $endereco_cidade,
                        'state'        => $endereco_uf,
                        'postal_code'  => $endereco_cep,
                        'country_code' => $endereco_pais_iso3166,
                    ],
                    'mmc'                   => $atividade_mcc,
                ]
            );
        }

        return $ret;
    }

    public function recuperarVendedorPorDocumento($documento)
    {
        return $this->_recuperarPessoaPorDocumento("sellers", $documento);
    }

    /*
     * Listar documentos de um vendedor
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sellers/:seller_id/documents
     * @param 20 $limit
     * @param time-descending $sort
     * @param int32 $offset
     * @param number $date_range
     * @param number $date_range_gt_
     * @param number $date_range_gte_
     * @param number $date_range_lt_
     * @param number $date_range_lte_
     */
    public function listarDocumentosDeUmVendedor($seller_id, $limit, $sort, $offset, $date_range, $date_range_gt_, $date_range_gte_, $date_range_lt_, $date_range_lte_)
    {
        $ret = $this->privateRequest(
            "GET",
            "/sellers/$seller_id/documents",
            [
                'limit'           => $limit,
                'sort'            => $sort,
                'offset'          => $offset,
                'date_range'      => $date_range,
                'date_range[gt]'  => $date_range_gt_,
                'date_range[gte]' => $date_range_gte_,
                'date_range[lt]'  => $date_range_lt_,
                'date_range[lte]' => $date_range_lte_]);
        return $ret;
    }

    public function recuperarDetalhesVendedor($id)
    {
        return $this->_recuperarDetalhesPessoa("sellers", $id);
    }

/*
 * Listando vendedores
 * @author Esteban D.Dortta
 * @todo FALTA IMPLEMENTAR
 * @access public
 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sellers
 * @param 20 $limit
 * @param time-descending $sort
 * @param int32 $offset
 * @param number $date_range
 * @param number $date_range_gt_
 * @param number $date_range_gte_
 * @param number $date_range_lt_
 * @param number $date_range_lte_
 */
    public function listarVendedores(
        $offset = 0,
        $limit = 20,
        $sort = null,
        $date_range = null,
        $date_range_gt_ = null,
        $date_range_gte_ = null,
        $date_range_lt_ = null,
        $date_range_lte_ = null) {
        $ret = $this->privateRequest(
            "GET",
            "/sellers",
            [
                'limit'           => $limit,
                'sort'            => $sort,
                'offset'          => $offset,
                'date_range'      => $date_range,
                'date_range[gt]'  => $date_range_gt_,
                'date_range[gte]' => $date_range_gte_,
                'date_range[lt]'  => $date_range_lt_,
                'date_range[lte]' => $date_range_lte_]);
        return $ret;
    }

    public function recuperarDetalhesComprador($id)
    {
        return $this->_recuperarDetalhesPessoa("buyers", $id);
    }

    public function recuperarCompradorPorDocumento($documento)
    {
        return $this->_recuperarPessoaPorDocumento("buyers", $documento);
    }

    public function listarCompradores($aParams)
    {
        $ret = $this->privateRequest(
            'GET',
            '/buyers',
            $aParams
        );

        return $ret;
    }

    public function criarCobrancaBoleto(
        $cCustomer,
        $cOnBehalfOf,
        $cReferenceId,
        $iAmount,
        $cDescription,
        $cInstrucoes = null,
        $cLogoURL = null,
        $dExpirationDate = null,
        $dPaymentLimitDate = null,
        $iLateFee = null,
        $iInterest = null,
        $dDiscountDate = null,
        $iDiscount = null) {
        return $this->_criarTransacaoSemToken(
            $cCustomer,
            $cOnBehalfOf,
            $cReferenceId,
            $iAmount,
            $cDescription,
            $dExpirationDate,
            $dPaymentLimitDate,
            $iLateFee,
            $iInterest,
            $dDiscountDate,
            $iDiscount,
            $cInstrucoes,
            $cLogoURL,
            null, null, null, null, null,

            true,
            'boleto',
            'BRL'
        );
    }

    /*
     * Criar documento de cadastro de vendedor
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sellers/:seller_id/documents
     * @param binary $file
     * @param string $category
     * @param string $metadata
     * @param string $description
     */
    public function criarDocumentoDeCadastroDeVendedor($seller_id, $file, $category, $metadata, $description)
    {
        $ret = $this->privateRequest(
            "POST",
            "/sellers/$seller_id/documents",
            [
                'file'        => $file,
                'category'    => $category,
                'metadata'    => $metadata,
                'description' => $description]);
        return $ret;
    }

    /*
     * Recuperar saldo de conta por seller
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sellers/:seller_id/balances
     * @param string $seller_id
     */
    public function recuperarSaldoDeContaPorSeller($seller_id)
    {
        $ret = $this->privateRequest(
            "GET",
            "/sellers/$seller_id/balances",
            []);
        return $ret;
    }

    /*
     * Lista contas por seller
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sellers/:seller_id/balances/all
     * @param string $seller_id
     */
    public function listaContasPorSeller($seller_id)
    {
        $ret = $this->privateRequest(
            "GET",
            "/sellers/$seller_id/balances/all",
            []);
        return $ret;
    }

    /*
     * Listar histórico de lançamentos de conta por seller
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sellers/:seller_id/balances/history
     * @param string $seller_id
     */
    public function listarHistoricoDeLancamentosDeContaPorSeller($seller_id)
    {
        $ret = $this->privateRequest(
            "GET",
            "/sellers/$seller_id/balances/history",
            []);
        return $ret;
    }

    /*
     * Criar novo vendedor do tipo indivíduo
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sellers/individuals
     * @param string $id
     * @param string $status
     * @param string $resource
     * @param string $type
     * @param number $account_balance
     * @param number $current_balance
     * @param string $fiscal_responsibility
     * @param string $first_name
     * @param string $last_name
     * @param string $email
     * @param string $phone_number
     * @param string $taxpayer_id
     * @param string $birthdate
     * @param string $description
     * @param Array $address
     * @param boolean $delinquent
     * @param string $default_debit
     * @param string $default_credit
     * @param string $mcc
     * @param object $metadata
     * @param dateTime $created_at
     * @param dateTime $updated_at
     */
    public function criarNovoVendedorDoTipoIndividuo($id, $status, $resource, $type, $account_balance, $current_balance, $fiscal_responsibility, $first_name, $last_name, $email, $phone_number, $taxpayer_id, $birthdate, $description, $address, $delinquent, $default_debit, $default_credit, $mcc, $metadata, $created_at, $updated_at)
    {
        $ret = $this->privateRequest(
            "POST",
            "/sellers/individuals",
            [
                'id'                    => $id,
                'status'                => $status,
                'resource'              => $resource,
                'type'                  => $type,
                'account_balance'       => $account_balance,
                'current_balance'       => $current_balance,
                'fiscal_responsibility' => $fiscal_responsibility,
                'first_name'            => $first_name,
                'last_name'             => $last_name,
                'email'                 => $email,
                'phone_number'          => $phone_number,
                'taxpayer_id'           => $taxpayer_id,
                'birthdate'             => $birthdate,
                'description'           => $description,
                'address'               => $address,
                'delinquent'            => $delinquent,
                'default_debit'         => $default_debit,
                'default_credit'        => $default_credit,
                'mcc'                   => $mcc,
                'metadata'              => $metadata,
                'created_at'            => $created_at,
                'updated_at'            => $updated_at]);
        return $ret;
    }

    /*
     * Criar novo vendedor do tipo empresa
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sellers/businesses
     * @param string $id
     * @param string $status
     * @param string $resource
     * @param string $type
     * @param number $account_balance
     * @param number $current_balance
     * @param string $fiscal_responsibility
     * @param Array $owner
     * @param string $description
     * @param string $business_name
     * @param string $business_phone
     * @param string $business_email
     * @param string $business_website
     * @param string $business_description
     * @param string $business_facebook
     * @param string $business_twitter
     * @param string $ein
     * @param Array $business_address
     * @param date $business_opening_date
     * @param Array $owner_address
     * @param boolean $delinquent
     * @param string $default_debit
     * @param string $default_credit
     * @param string $mcc
     * @param object $metadata
     * @param dateTime $created_at
     * @param dateTime $updated_at
     */
    public function criarNovoVendedorDoTipoEmpresa($id, $status, $resource, $type, $account_balance, $current_balance, $fiscal_responsibility, $owner, $description, $business_name, $business_phone, $business_email, $business_website, $business_description, $business_facebook, $business_twitter, $ein, $business_address, $business_opening_date, $owner_address, $delinquent, $default_debit, $default_credit, $mcc, $metadata, $created_at, $updated_at)
    {
        $ret = $this->privateRequest(
            "POST",
            "/sellers/businesses",
            [
                'id'                    => $id,
                'status'                => $status,
                'resource'              => $resource,
                'type'                  => $type,
                'account_balance'       => $account_balance,
                'current_balance'       => $current_balance,
                'fiscal_responsibility' => $fiscal_responsibility,
                'owner'                 => $owner,
                'description'           => $description,
                'business_name'         => $business_name,
                'business_phone'        => $business_phone,
                'business_email'        => $business_email,
                'business_website'      => $business_website,
                'business_description'  => $business_description,
                'business_facebook'     => $business_facebook,
                'business_twitter'      => $business_twitter,
                'ein'                   => $ein,
                'business_address'      => $business_address,
                'business_opening_date' => $business_opening_date,
                'owner_address'         => $owner_address,
                'delinquent'            => $delinquent,
                'default_debit'         => $default_debit,
                'default_credit'        => $default_credit,
                'mcc'                   => $mcc,
                'metadata'              => $metadata,
                'created_at'            => $created_at,
                'updated_at'            => $updated_at]);
        return $ret;
    }

    /*
     * Alterar um vendedor do tipo empresa
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sellers/businesses/:id
     * @param string $id
     */
    public function alterarUmVendedorDoTipoEmpresa($id)
    {
        $ret = $this->privateRequest(
            "PUT",
            "/sellers/businesses/$id",
            []);
        return $ret;
    }

    /*
     * Recupera detalhes de vendedor pelo id
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sellers/:id
     * @param string $id
     */
    public function recuperaDetalhesDeVendedorPeloId($id)
    {
        $ret = $this->privateRequest(
            "GET",
            "/sellers/$id",
            []);
        return $ret;
    }

    /*
     * Deleta um de vendedor pelo id
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sellers/:id
     * @param string $id
     */
    public function deletaUmDeVendedorPeloId($id)
    {
        $ret = $this->privateRequest(
            "DELETE",
            "/sellers/$id",
            []);
        return $ret;
    }

    /*
     * Atualiza os dados de um documento de um vendedor
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sellers/documents/:id
     * @param string $id
     */
    public function atualizaOsDadosDeUmDocumentoDeUmVendedor($id)
    {
        $ret = $this->privateRequest(
            "PUT",
            "/sellers/documents/$id",
            []);
        return $ret;
    }

    /*
     * Lista contas por buyer
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/buyers/:buyer_id/balances
     * @param string $buyer_id
     */
    public function listaContasPorBuyer($buyer_id)
    {
        $ret = $this->privateRequest(
            "GET",
            "/buyers/$buyer_id/balances",
            []);
        return $ret;
    }

    /*
     * Listar histórico de lançamentos de conta por buyer
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/buyers/:buyer_id/balances/history
     * @param string $buyer_id
     */
    public function listarHistoricoDeLancamentosDeContaPorBuyer($buyer_id)
    {
        $ret = $this->privateRequest(
            "GET",
            "/buyers/$buyer_id/balances/history",
            []);
        return $ret;
    }

    /*
     * Alterar detalhes de comprador
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/buyers/:buyer_id
     * @param string $buyer_id
     */
    public function alterarDetalhesDeComprador($buyer_id)
    {
        $ret = $this->privateRequest(
            "PUT",
            "/buyers/$buyer_id",
            []);
        return $ret;
    }

    /*
     * Recuperar detalhes de comprador
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/buyers/:buyer_id
     * @param 3c7378501aed4f8497b1891af9b84a71 $buyer_id
     */
    public function recuperarDetalhesDeComprador($buyer_id)
    {
        $ret = $this->privateRequest(
            "GET",
            "/buyers/$buyer_id",
            []);
        return $ret;
    }

    /*
     * Remover comprador pelo identificador
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/buyers/:buyer_id
     * @param string $buyer_id
     */
    public function removerCompradorPeloIdentificador($buyer_id)
    {
        $ret = $this->privateRequest(
            "DELETE",
            "/buyers/$buyer_id",
            []);
        return $ret;
    }

    /*
     * Criar comprador
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/buyers
     */
    public function criarComprador()
    {
        $ret = $this->privateRequest(
            "POST",
            "/buyers",
            []);
        return $ret;
    }

    /*
     * Listar comprador por marketplace
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/buyers
     * @param 3400 $offset
     */
    public function listarCompradorPorMarketplace($offset)
    {
        $ret = $this->privateRequest(
            "GET",
            "/buyers",
            [
                'offset' => $offset]);
        return $ret;
    }

    /*
     * Buscar comprador por CPF/CNPJ
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/buyers/search
     * @param 06778549947 $taxerpayer_id
     */
    public function buscarcompradorporcpfCnpj($taxerpayer_id)
    {
        $ret = $this->privateRequest(
            "GET",
            "/buyers/search",
            [
                'taxerpayer_id' => $taxerpayer_id]);
        return $ret;
    }

    /*
     * Listar histórico de lançamentos pelo identificador da conta
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/balances/:balance_id/history
     * @param string $balance_id
     */
    public function listarHistoricoDeLancamentosPeloIdentificadorDaConta($balance_id)
    {
        $ret = $this->privateRequest(
            "GET",
            "/balances/$balance_id/history",
            []);
        return $ret;
    }

    /*
     * Criar novo token de cartão
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/cards/tokens
     * @param string $holder_name
     * @param string $expiration_month
     * @param string $expiration_year
     * @param string $card_number
     * @param string $security_code
     */
    public function criarNovoTokenDeCartao($holder_name, $expiration_month, $expiration_year, $card_number, $security_code)
    {
        $ret = $this->privateRequest(
            "POST",
            "/cards/tokens",
            [
                'holder_name'      => $holder_name,
                'expiration_month' => $expiration_month,
                'expiration_year'  => $expiration_year,
                'card_number'      => $card_number,
                'security_code'    => $security_code]);
        return $ret;
    }

    /*
     * Criar novo token de conta bancária
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/bank_accounts/tokens
     * @param string $holder_name
     * @param number $bank_code
     * @param number $routing_number
     * @param number $account_number
     * @param string $taxpayer_id
     * @param string $ein
     * @param string $type
     */
    public function criarNovoTokenDeContaBancaria($holder_name, $bank_code, $routing_number, $account_number, $taxpayer_id, $ein, $type)
    {
        $ret = $this->privateRequest(
            "POST",
            "/bank_accounts/tokens",
            [
                'holder_name'    => $holder_name,
                'bank_code'      => $bank_code,
                'routing_number' => $routing_number,
                'account_number' => $account_number,
                'taxpayer_id'    => $taxpayer_id,
                'ein'            => $ein,
                'type'           => $type]);
        return $ret;
    }

    /*
     * Recuperar detalhes de token de cartão/conta bancária
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/tokens/:token_id
     * @param string $token_id
     */
    public function recuperardetalhesdetokendecartaoContabancaria($token_id)
    {
        $ret = $this->privateRequest(
            "GET",
            "/tokens/$token_id",
            []);
        return $ret;
    }

    /*
     * Listar MCCs (Merchant Category Codes)
     * @author Esteban D.Dortta
     * @todo Verificar parâmetros aceitáveis: offset e limit
     * @access public
     * @link {{baseUrl}}/v1/merchant_category_codes
     */
    public function listarMccsMerchantCategoryCodes($aParams = [])
    {
        $ret = $this->publicRequest(
            "GET",
            "/merchant_category_codes",
            $aParams);
        return $ret;
    }

}
