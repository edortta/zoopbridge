<?php

require_once dirname(__FILE__) . "/MarketplaceZoop_NotificacoesEEventos.php";

class UsuariosEApiKeysZoop extends NotificacoesEEventosZoop
{

    /*
     * Permissão do usuário por permission_id
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/users/:user_id/permissions/:permission_id
     * @param string $permission_id
     */
    public function permissaodousuarioporpermissionId($permission_id)
    {
        $ret = $this->publicRequest(
            "GET",
            "",
            []);
        return $ret;
    }

    /*
     * deleta uma permissão
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/users/:user_id/permissions/:permission_id
     * @param string $permission_id
     */
    public function deletaUmaPermissao($permission_id)
    {
        $ret = $this->publicRequest(
            "DELETE",
            "",
            []);
        return $ret;
    }

    /*
     * Permissão do usuário
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/users/:user_id/permissions
     * @param 20 $limit
     * @param time-descending $sort
     * @param int32 $offset
     * @param number $date_range
     * @param number $date_range_gt_
     * @param number $date_range_gte_
     * @param number $date_range_lt_
     * @param number $date_range_lte_
     */
    public function permissaoDoUsuario($limit, $sort, $offset, $date_range, $date_range_gt_, $date_range_gte_, $date_range_lt_, $date_range_lte_)
    {
        $ret = $this->publicRequest(
            "GET",
            "",
            [
                'limit'           => $limit,
                'sort'            => $sort,
                'offset'          => $offset,
                'date_range'      => $date_range,
                'date_range[gt]'  => $date_range_gt_,
                'date_range[gte]' => $date_range_gte_,
                'date_range[lt]'  => $date_range_lt_,
                'date_range[lte]' => $date_range_lte_]);
        return $ret;
    }

    /*
     * Criar permissão para usuário
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/users/:user_id/permissions
     * @param string $user
     * @param string $service
     * @param string $action
     * @param string $resource
     * @param string $marketplace
     * @param string $customer
     */
    public function criarPermissaoParaUsuario($user, $service, $action, $resource, $marketplace, $customer)
    {
        $ret = $this->publicRequest(
            "POST",
            "",
            [
                'user'        => $user,
                'service'     => $service,
                'action'      => $action,
                'resource'    => $resource,
                'marketplace' => $marketplace,
                'customer'    => $customer]);
        return $ret;
    }

    /*
     * Recuperar detalhes de um API Key
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/api_keys/:apikey_id
     * @param string $apikey_id
     */
    public function recuperarDetalhesDeUmApiKey($apikey_id)
    {
        $ret = $this->privateRequest(
            "GET",
            "/api_keys/$apikey_id",
            []);
        return $ret;
    }

    /*
     * Remover API Key
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/api_keys/:apikey_id
     * @param string $apikey_id
     */
    public function removerApiKey($apikey_id)
    {
        $ret = $this->privateRequest(
            "DELETE",
            "/api_keys/$apikey_id",
            []);
        return $ret;
    }

    /*
     * Criar nova API Key por marketplace
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/api_keys
     * @param string $name
     * @param object $metadata
     */
    public function criarNovaApiKeyPorMarketplace($name, $metadata)
    {
        $ret = $this->privateRequest(
            "POST",
            "/api_keys",
            [
                'name'     => $name,
                'metadata' => $metadata]);
        return $ret;
    }

    /*
     * Listar chaves de API por marketplace
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/api_keys
     * @param 20 $limit
     * @param time-descending $sort
     * @param int32 $offset
     * @param number $date_range
     * @param number $date_range_gt_
     * @param number $date_range_gte_
     * @param number $date_range_lt_
     * @param number $date_range_lte_
     */
    public function listarChavesDeApiPorMarketplace($limit, $sort, $offset, $date_range, $date_range_gt_, $date_range_gte_, $date_range_lt_, $date_range_lte_)
    {
        $ret = $this->privateRequest(
            "GET",
            "/api_keys",
            [
                'limit'           => $limit,
                'sort'            => $sort,
                'offset'          => $offset,
                'date_range'      => $date_range,
                'date_range[gt]'  => $date_range_gt_,
                'date_range[gte]' => $date_range_gte_,
                'date_range[lt]'  => $date_range_lt_,
                'date_range[lte]' => $date_range_lte_]);
        return $ret;
    }

    /*
     * Recuperar detalhes do marketplace
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id
     */
    public function recuperarDetalhesDoMarketplace()
    {
        $ret = $this->privateRequest(
            "GET",
            "",
            []);
        return $ret;
    }

    /*
     * Criar novo usuário de API por marketplace
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/users
     * @param string $email
     * @param string $username
     * @param string $password
     * @param string $first_name
     * @param string $last_name
     * @param object $metadata
     */
    public function criarNovoUsuarioDeApiPorMarketplace($email, $username, $password, $first_name, $last_name, $metadata)
    {
        $ret = $this->privateRequest(
            "POST",
            "/users",
            [
                'email'      => $email,
                'username'   => $username,
                'password'   => $password,
                'first_name' => $first_name,
                'last_name'  => $last_name,
                'metadata'   => $metadata]);
        return $ret;
    }

    /*
     * Criar novo marketplace
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces
     */
    public function criarNovoMarketplace()
    {
        $ret = $this->publicRequest(
            "POST",
            "/marketplaces",
            []);
        return $ret;
    }

    /*
     * Alterar detalhes de usuário
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/users/:user_id
     */
    public function alterarDetalhesDeUsuario()
    {
        $ret = $this->publicRequest(
            "PUT",
            "/users/$user_id",
            []);
        return $ret;
    }

    /*
     * Recuperar detalhes de usuário
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/users/:user_id
     */
    public function recuperarDetalhesDeUsuario($user_id)
    {
        $ret = $this->publicRequest(
            "GET",
            "/users/$user_id",
            []);
        return $ret;
    }

    /*
     * Remover usuário
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/users/:user_id
     */
    public function removerUsuario()
    {
        $ret = $this->publicRequest(
            "DELETE",
            "/users/$user_id",
            []);
        return $ret;
    }

    /*
     * Criar novo usuário de API
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/users
     * @param string $email
     * @param string $username
     * @param string $password
     * @param string $first_name
     * @param string $last_name
     * @param object $metadata
     */
    public function criarNovoUsuarioDeApi($email, $username, $password, $first_name, $last_name, $metadata)
    {
        $ret = $this->publicRequest(
            "POST",
            "/users",
            [
                'email'      => $email,
                'username'   => $username,
                'password'   => $password,
                'first_name' => $first_name,
                'last_name'  => $last_name,
                'metadata'   => $metadata]);
        return $ret;
    }

    /*
     * Relizar login por usuário/senha
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/users/signin
     * @param string $username
     * @param string $password
     */
    public function relizarloginporusuarioSenha($username, $password)
    {
        $ret = $this->publicRequest(
            "POST",
            "/users/signin",
            [
                'username' => $username,
                'password' => $password]);
        return $ret;
    }

}
