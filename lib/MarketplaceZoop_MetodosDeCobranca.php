<?php

require_once dirname(__FILE__) . "/MarketplaceZoop_UsuariosEApiKeys.php";

class MetodosDeCobrancaZoop extends UsuariosEApiKeysZoop {

    /*
     * Recupera detalhes de regra de divisão por transação
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/transactions/:transaction_id/split_rules/:id
     * @param string $transaction_id
     * @param string $id
     */
    public function recuperaDetalhesDeRegraDeDivisaoPorTransacao($transaction_id, $id) {
        $ret = $this->privateRequest(
            "GET",
            "/transactions/$transaction_id/split_rules/$id",
            []);
        return $ret;
    }

    /*
     * Alterar regra de divisão por transação
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/transactions/:transaction_id/split_rules/:id
     * @param string $transaction_id
     * @param string $id
     */
    public function alterarRegraDeDivisaoPorTransacao($transaction_id, $id) {
        $ret = $this->privateRequest(
            "PUT",
            "/transactions/$transaction_id/split_rules/$id",
            []);
        return $ret;
    }

    /*
     * Remover regra de divisão por transação
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/transactions/:transaction_id/split_rules/:id
     * @param string $transaction_id
     * @param string $id
     */
    public function removerRegraDeDivisaoPorTransacao($transaction_id, $id) {
        $ret = $this->privateRequest(
            "DELETE",
            "/transactions/$transaction_id/split_rules/$id",
            []);
        return $ret;
    }

    /*
     * Recuperar detalhes de regra de divisão por transação
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/transactions/:transaction_id/split_rules
     * @param string $transaction_id
     */
    public function recuperarDetalhesDeRegraDeDivisaoPorTransacao($transaction_id) {
        $ret = $this->privateRequest(
            "GET",
            "/transactions/$transaction_id/split_rules",
            []);
        return $ret;
    }

    /*
     * Criar regra de divisão por transação
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/transactions/:transaction_id/split_rules
     * @param integer $amount
     * @param string $recipient
     * @param boolean $liable
     * @param boolean $charge_processing_fee
     * @param integer $percentage
     */
    public function criarRegraDeDivisaoPorTransacao($amount, $recipient, $liable, $charge_processing_fee, $percentage) {
        $ret = $this->privateRequest(
            "POST",
            "/transactions/$transaction_id/split_rules",
            [
                'amount'                => $amount,
                'recipient'             => $recipient,
                'liable'                => $liable,
                'charge_processing_fee' => $charge_processing_fee,
                'percentage'            => $percentage]);
        return $ret;
    }

    /*
     * Recuperar detalhes de transação pelo identificador
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/transactions/:transaction_id
     * @param string $transaction_id
     */
    public function recuperarDetalhesDeTransacaoPeloIdentificador($transaction_id) {
        $ret = $this->privateRequest(
            "GET",
            "/transactions/$transaction_id",
            []);
        return $ret;
    }

    /*
     * Alterar detalhes de transação pelo identificador
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/transactions/:transaction_id
     * @param string $transaction_id
     */
    public function alterarDetalhesDeTransacaoPeloIdentificador($transaction_id) {
        $ret = $this->privateRequest(
            "PUT",
            "/transactions/$transaction_id",
            []);
        return $ret;
    }

    /*
     * Estornar transação cartão não presente
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/transactions/:transaction_id/void
     * @param string $on_behalf_of
     * @param integer $amount
     */
    public function estornarTransacaoCartaoNaoPresente($on_behalf_of, $amount) {
        $ret = $this->privateRequest(
            "POST",
            "/transactions/$transaction_id/void",
            [
                'on_behalf_of' => $on_behalf_of,
                'amount'       => $amount]);
        return $ret;
    }

    /*
     * Capturar transação cartão não presente
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/transactions/:transaction_id/capture
     * @param string $on_behalf_of
     * @param integer $amount
     */
    public function capturarTransacaoCartaoNaoPresente($on_behalf_of, $amount) {
        $ret = $this->privateRequest(
            "POST",
            "/transactions/$transaction_id/capture",
            [
                'on_behalf_of' => $on_behalf_of,
                'amount'       => $amount]);
        return $ret;
    }

    /*
     * Listar transaçoes do marketplace
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/transactions
     * @param 20 $limit
     * @param time-descending $sort
     * @param int32 $offset
     * @param number $date_range
     * @param number $date_range_gt_
     * @param number $date_range_gte_
     * @param number $date_range_lt_
     * @param number $date_range_lte_
     * @param string $reference_id
     * @param string $status
     * @param string $payment_type
     */
    public function listarTransacoesDoMarketplace($limit, $sort, $offset, $date_range, $date_range_gt_, $date_range_gte_, $date_range_lt_, $date_range_lte_, $reference_id, $status, $payment_type) {
        $ret = $this->privateRequest(
            "GET",
            "/transactions",
            [
                'limit'           => $limit,
                'sort'            => $sort,
                'offset'          => $offset,
                'date_range'      => $date_range,
                'date_range[gt]'  => $date_range_gt_,
                'date_range[gte]' => $date_range_gte_,
                'date_range[lt]'  => $date_range_lt_,
                'date_range[lte]' => $date_range_lte_,
                'reference_id'    => $reference_id,
                'status'          => $status,
                'payment_type'    => $payment_type]);
        return $ret;
    }

    /*
     * Criar transação Cartão Não Presente
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/transactions
     * @param integer $amount
     * @param string $currency
     * @param string $description
     * @param string $payment_type
     * @param string $on_behalf_of
     * @param boolean $capture
     * @param string $reference_id
     * @param Array $payment_method
     * @param Array $source
     * @param Array $installment_plan
     * @param string $statement_descriptor
     * @param string $customer
     * @param string $token
     * @param object $metadata
     */
    public function criarTransacaoCartaoNaoPresente($amount, $currency, $description, $payment_type, $on_behalf_of, $capture, $reference_id, $payment_method, $source, $installment_plan, $statement_descriptor, $customer, $token, $metadata) {
        $ret = $this->privateRequest(
            "POST",
            "/transactions",
            [
                'amount'               => $amount,
                'currency'             => $currency,
                'description'          => $description,
                'payment_type'         => $payment_type,
                'on_behalf_of'         => $on_behalf_of,
                'capture'              => $capture,
                'reference_id'         => $reference_id,
                'payment_method'       => $payment_method,
                'source'               => $source,
                'installment_plan'     => $installment_plan,
                'statement_descriptor' => $statement_descriptor,
                'customer'             => $customer,
                'token'                => $token,
                'metadata'             => $metadata]);
        return $ret;
    }

    /*
     * Criar transação do tipo boleto
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/transactions
     * @param 10050 $amount
     * @param BRL $currency
     * @param Teste de emissão de boleto $description
     * @param 99fa6cb549ef12f869fbcc4314ea4017 $reference_id
     * @param 5b0bad5f3635499a8edd84a6f027d155 $on_behalf_of
     * @param 3c7378501aed4f8497b1891af9b84a71 $customer
     * @param boleto $payment_type
     * @param 1 $capture
     * @param Array $payment_method
     */
    public function criarTransacaoDoTipoBoleto($amount, $currency, $description, $reference_id, $on_behalf_of, $customer, $payment_type, $capture, $payment_method) {
        $ret = $this->privateRequest(
            "POST",
            "/transactions",
            [
                'amount'         => $amount,
                'currency'       => $currency,
                'description'    => $description,
                'reference_id'   => $reference_id,
                'on_behalf_of'   => $on_behalf_of,
                'customer'       => $customer,
                'payment_type'   => $payment_type,
                'capture'        => $capture,
                'payment_method' => $payment_method,
            ]);
        return $ret;
    }

    /*
     * Recuperar detalhes de cartão pelo identificador
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/cards/:card_id
     * @param string $card_id
     */
    public function recuperarDetalhesDeCartaoPeloIdentificador($card_id) {
        $ret = $this->privateRequest(
            "GET",
            "/cards/$card_id",
            []);
        return $ret;
    }

    /*
     * Remover cartão pelo identificador
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/cards/:card_id
     * @param string $card_id
     */
    public function removerCartaoPeloIdentificador($card_id) {
        $ret = $this->privateRequest(
            "DELETE",
            "/cards/$card_id",
            []);
        return $ret;
    }

    /*
     * Atualizar detalhes de cartão pelo identificador
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/cards/:card_id
     * @param string $card_id
     */
    public function atualizarDetalhesDeCartaoPeloIdentificador($card_id) {
        $ret = $this->privateRequest(
            "PUT",
            "/cards/$card_id",
            []);
        return $ret;
    }

    /*
     * Associar cartão com comprador
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/cards
     * @param string $token
     * @param string $customer
     */
    public function associarCartaoComComprador($token, $customer) {
        $ret = $this->privateRequest(
            "POST",
            "/cards",
            [
                'token'    => $token,
                'customer' => $customer]);
        return $ret;
    }

    /*
     * Recuperar detalhes de source tão pelo identificador
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sources/:source_id
     * @param string $source_id
     */
    public function recuperarDetalhesDeSourceTaoPeloIdentificador($source_id) {
        $ret = $this->privateRequest(
            "GET",
            "/sources/$source_id",
            []);
        return $ret;
    }

    /*
     * Remover source pelo identificador
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sources/:source_id
     * @param string $source_id
     */
    public function removerSourcePeloIdentificador($source_id) {
        $ret = $this->privateRequest(
            "DELETE",
            "/sources/$source_id",
            []);
        return $ret;
    }

    /*
     * Criar source para utilizacão transação
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sources
     * @param string $type
     * @param single_use $usage
     * @param integer $amount
     * @param string $currency
     * @param string $description
     * @param boolean $capture
     * @param string $on_behalf_of
     * @param string $reference_id
     * @param Array $card
     * @param Array $cards
     * @param Array $installment_plan
     * @param string $statement_descriptor
     * @param Array $customer
     * @param Array $token
     * @param object $metadata
     */
    public function criarSourceParaUtilizacaoTransacao($type, $usage, $amount, $currency, $description, $capture, $on_behalf_of, $reference_id, $card, $cards, $installment_plan, $statement_descriptor, $customer, $token, $metadata) {
        $ret = $this->privateRequest(
            "POST",
            "/sources",
            [
                'type'                 => $type,
                'usage'                => $usage,
                'amount'               => $amount,
                'currency'             => $currency,
                'description'          => $description,
                'capture'              => $capture,
                'on_behalf_of'         => $on_behalf_of,
                'reference_id'         => $reference_id,
                'card'                 => $card,
                'cards'                => $cards,
                'installment_plan'     => $installment_plan,
                'statement_descriptor' => $statement_descriptor,
                'customer'             => $customer,
                'token'                => $token,
                'metadata'             => $metadata]);
        return $ret;
    }

    /*
     * Recuperar detalhes do recibo
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/receipts/:receipt_id
     * @param string $receipt_id
     */
    public function recuperarDetalhesDoRecibo($receipt_id) {
        $ret = $this->privateRequest(
            "GET",
            "/receipts/$receipt_id",
            []);
        return $ret;
    }

    /*
     * Alterar detalhes do recibo
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/receipts/:receipt_id
     * @param string $receipt_id
     */
    public function alterarDetalhesDoRecibo($receipt_id) {
        $ret = $this->privateRequest(
            "PUT",
            "/receipts/$receipt_id",
            []);
        return $ret;
    }

    /*
     * Enviar recibo por email
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/receipts/:receipt_id/emails
     */
    public function enviarReciboPorEmail() {
        $ret = $this->privateRequest(
            "POST",
            "/receipts/$receipt_id/emails",
            []);
        return $ret;
    }

    /*
     * Enviar recibo por SMS
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/receipts/:receipt_id/texts
     */
    public function enviarReciboPorSms() {
        $ret = $this->privateRequest(
            "POST",
            "/receipts/$receipt_id/texts",
            []);
        return $ret;
    }

    /*
     * Enviar recibo por sms.email
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/receipts/:receipt_id/send
     */
    public function enviarReciboPorSmsOuEmail() {
        $ret = $this->privateRequest(
            "POST",
            "/receipts/$receipt_id/send",
            []);
        return $ret;
    }

    /*
     * Recuperar detalhes de boleto
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/boletos/:boleto_id
     * @param string $boleto_id
     */
    public function recuperarDetalhesDeBoleto($boleto_id) {
        $ret = $this->privateRequest(
            "GET",
            "/boletos/$boleto_id",
            []);
        return $ret;
    }

    /*
     * Enviar cobrança de boleto por email
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/boletos/:boleto_id/emails
     */
    public function enviarCobrancaDeBoletoPorEmail() {
        $ret = $this->privateRequest(
            "POST",
            "/boletos/$boleto_id/emails",
            []);
        return $ret;
    }

    /*
     * Listar transaçoes por vendedor
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sellers/:seller_id/transactions
     * @param 20 $limit
     * @param time-descending $sort
     * @param int32 $offset
     * @param number $date_range
     * @param number $date_range_gt_
     * @param number $date_range_gte_
     * @param number $date_range_lt_
     * @param number $date_range_lte_
     */
    public function listarTransacoesPorVendedor($limit, $sort, $offset, $date_range, $date_range_gt_, $date_range_gte_, $date_range_lt_, $date_range_lte_) {
        $ret = $this->privateRequest(
            "GET",
            "/sellers/$seller_id/transactions",
            [
                'limit'           => $limit,
                'sort'            => $sort,
                'offset'          => $offset,
                'date_range'      => $date_range,
                'date_range[gt]'  => $date_range_gt_,
                'date_range[gte]' => $date_range_gte_,
                'date_range[lt]'  => $date_range_lt_,
                'date_range[lte]' => $date_range_lte_]);
        return $ret;
    }

    /*
     * Alterar detalhes de terminal pelo identificador
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/card-present/terminals/:terminal_id
     */
    public function alterarDetalhesDeTerminalPeloIdentificador() {
        $ret = $this->publicRequest(
            "PUT",
            "/card-present/terminals/$terminal_id",
            []);
        return $ret;
    }

    /*
     * Recuperar detalhes de terminal pelo identificador
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/card-present/terminals/:terminal_id
     */
    public function recuperarDetalhesDeTerminalPeloIdentificador() {
        $ret = $this->publicRequest(
            "GET",
            "/card-present/terminals/$terminal_id",
            []);
        return $ret;
    }

    /*
     * Remover terminal pelo identificador
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/card-present/terminals/:terminal_id
     */
    public function removerTerminalPeloIdentificador() {
        $ret = $this->publicRequest(
            "DELETE",
            "/card-present/terminals/$terminal_id",
            []);
        return $ret;
    }

    /*
     * Recuperar parâmetros de inicialização por terminal
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/card-present/terminals/:terminal_id/logon
     */
    public function recuperarParametrosDeInicializacaoPorTerminal() {
        $ret = $this->publicRequest(
            "POST",
            "/card-present/terminals/$terminal_id/logon",
            []);
        return $ret;
    }

    /*
     * Criar novo terminal
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/card-present/terminals
     */
    public function criarNovoTerminal() {
        $ret = $this->publicRequest(
            "POST",
            "/card-present/terminals",
            []);
        return $ret;
    }

    /*
     * Buscar terminal pelo serial number
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/card-present/terminals/search
     */
    public function buscarTerminalPeloSerialNumber() {
        $ret = $this->publicRequest(
            "GET",
            "/card-present/terminals/search",
            []);
        return $ret;
    }

    /*
     * Criar transação cartão presente
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/card-present/marketplaces/:marketplace_id/sellers/:seller_id/transactions
     */
    public function criarTransacaoCartaoPresente() {
        $ret = $this->privateRequest(
            "POST",
            "/sellers/$seller_id/transactions",
            []);
        return $ret;
    }

    /*
     * Criar transação de pre-autorização cartão presente
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/card-present/marketplaces/:marketplace_id/sellers/:seller_id/preauthorizations
     */
    public function criarTransacaoDePreAutorizacaoCartaoPresente() {
        $ret = $this->privateRequest(
            "POST",
            "/sellers/$seller_id/preauthorizations",
            []);
        return $ret;
    }

    /*
     * Processar aviso de falha transação cartão presente
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/card-present/marketplaces/:marketplace_id/sellers/:seller_id/advices
     */
    public function processarAvisoDeFalhaTransacaoCartaoPresente() {
        $ret = $this->privateRequest(
            "POST",
            "/sellers/$seller_id/advices",
            []);
        return $ret;
    }

    /*
     * Capturar transação de pre-autorização cartão presente
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/card-present/marketplaces/:marketplace_id/preauthorizations/:transaction_id/capture
     */
    public function capturarTransacaoDePreAutorizacaoCartaoPresente() {
        $ret = $this->privateRequest(
            "POST",
            "/preauthorizations/$transaction_id/capture",
            []);
        return $ret;
    }

    /*
     * Estornar transação de pre-autorização cartão presente
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/card-present/marketplaces/:marketplace_id/preauthorizations/:transaction_id/void
     */
    public function estornarTransacaoDePreAutorizacaoCartaoPresente() {
        $ret = $this->privateRequest(
            "POST",
            "/preauthorizations/$transaction_id/void",
            []);
        return $ret;
    }

    /*
     * Desfazer transação de pre-autorização cartão presente
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/card-present/marketplaces/:marketplace_id/preauthorizations/:transaction_id/reversals
     */
    public function desfazerTransacaoDePreAutorizacaoCartaoPresente() {
        $ret = $this->privateRequest(
            "POST",
            "/preauthorizations/$transaction_id/reversals",
            []);
        return $ret;
    }

    /*
     * Estornar transação cartão presente
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/card-present/marketplaces/:marketplace_id/transactions/:transaction_id/void
     */
    public function estornarTransacaoCartaoPresente() {
        $ret = $this->privateRequest(
            "POST",
            "/transactions/$transaction_id/void",
            []);
        return $ret;
    }

    /*
     * Desfazer transação de cartão presente
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/card-present/marketplaces/:marketplace_id/transactions/:transaction_id/reversals
     */
    public function desfazerTransacaoDeCartaoPresente() {
        $ret = $this->privateRequest(
            "POST",
            "/transactions/$transaction_id/reversals",
            []);
        return $ret;
    }

    /*
     * Renderizar template de recibo HTML
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1/receipts/:marketplace_id/:receipt_id
     * @param string $receipt_id
     */
    public function renderizarTemplateDeReciboHtml($receipt_id) {
        $ret = $this->privateRequest(
            "GET",
            "/$receipt_id",
            []);
        return $ret;
    }

    /*
     * Criar transação cartão presente
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1.1/card-present/marketplaces/:marketplace_id/sellers/:seller_id/transactions
     */
    public function criarTransacaoCartaoPresente1_1() {
        $ret = $this->privateRequest(
            "POST",
            "/sellers/$seller_id/transactions",
            []);
        return $ret;
    }

    /*
     * Confirmar transação cartão presente
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v1.1/card-present/marketplaces/:marketplace_id/transactions/:transaction_id
     * @param string $transaction_id
     */
    public function confirmarTransacaoCartaoPresente($transaction_id) {
        $ret = $this->privateRequest(
            "PUT",
            "/transactions/$transaction_id",
            []);
        return $ret;
    }

    /*
     * Recuperar parâmetros de inicialização por terminal
     * @author Esteban D.Dortta
     * @todo FALTA IMPLEMENTAR
     * @access public
     * @link {{baseUrl}}/v2/card-present/terminals/:terminal_id/logon
     */
    public function recuperarParametrosDeInicializacaoPorTerminal_2() {
        $ret = $this->publicRequest(
            "POST",
            "/card-present/terminals/$terminal_id/logon",
            []);
        return $ret;
    }

}
?>