<?php

require_once "MarketplaceZoop_PlanosEAssinaturas.php";

class PagamentosETransferenciasZoop extends PlanosEAssinaturasZoop {

	/*
		 * Recuperar política de recebimento por seller
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sellers/:seller_id/receiving_policy
		 * @param string $seller_id
	*/
	public function recuperarPoliticaDeRecebimentoPorSeller($seller_id) {
		$ret = $this->privateRequest(
			"GET",
			"/sellers/$seller_id/receiving_policy",
			[]);
		return $ret;
	}

	/*
		 * Alterar política de recebimento por seller
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sellers/:seller_id/receiving_policy
		 * @param daily $transfer_interval
		 * @param integer $transfer_day
		 * @param boolean $transfer_enabled
		 * @param integer $minimum_transfer_value
	*/
	public function alterarPoliticaDeRecebimentoPorSeller($transfer_interval, $transfer_day, $transfer_enabled, $minimum_transfer_value) {
		$ret = $this->privateRequest(
			"POST",
			"/sellers/$seller_id/receiving_policy",
			[
				'transfer_interval'      => $transfer_interval,
				'transfer_day'           => $transfer_day,
				'transfer_enabled'       => $transfer_enabled,
				'minimum_transfer_value' => $minimum_transfer_value]);
		return $ret;
	}

	/*
		 * Listar transferências por seller
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sellers/:seller_id/transfers
		 * @param string $seller_id
	*/
	public function listarTransferenciasPorSeller($seller_id) {
		$ret = $this->privateRequest(
			"GET",
			"/sellers/$seller_id/transfers",
			[]);
		return $ret;
	}

	/*
		 * Listar contas bancárias por seller
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sellers/:seller_id/bank_accounts
		 * @param string $seller_id
	*/
	public function listarContasBancariasPorSeller($seller_id) {
		$ret = $this->privateRequest(
			"GET",
			"/sellers/$seller_id/bank_accounts",
			[]);
		return $ret;
	}

	/*
		 * Listar recebíveis por seller
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sellers/:seller_id/receivables
		 * @param string $seller_id
	*/
	public function listarRecebiveisPorSeller($seller_id) {
		$ret = $this->privateRequest(
			"GET",
			"/sellers/$seller_id/receivables",
			[]);
		return $ret;
	}

	/*
		 * Recuperar assinatura de plano de venda por seller
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/sellers/:seller_id/subscriptions
		 * @param string $seller_id
	*/
	public function recuperarAssinaturaDePlanoDeVendaPorSeller($seller_id) {
		$ret = $this->privateRequest(
			"GET",
			"/sellers/$seller_id/subscriptions",
			[]);
		return $ret;
	}

	/*
		 * Recuperar detalhes de conta bancária por identificador
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/bank_accounts/:bank_account_id
		 * @param string $bank_account_id
	*/
	public function recuperarDetalhesDeContaBancariaPorIdentificador($bank_account_id) {
		$ret = $this->privateRequest(
			"GET",
			"/bank_accounts/$bank_account_id",
			[]);
		return $ret;
	}

	/*
		 * Alterar detalhes de conta bancária
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/bank_accounts/:bank_account_id
		 * @param string $bank_account_id
	*/
	public function alterarDetalhesDeContaBancaria($bank_account_id) {
		$ret = $this->privateRequest(
			"PUT",
			"/bank_accounts/$bank_account_id",
			[]);
		return $ret;
	}

	/*
		 * Eliminar detalhes de conta bancária
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/bank_accounts/:bank_account_id
		 * @param string $bank_account_id
	*/
	public function eliminarDetalhesDeContaBancaria($bank_account_id) {
		$ret = $this->privateRequest(
			"DELETE",
			"/bank_accounts/$bank_account_id",
			[]);
		return $ret;
	}

	/*
		 * Criar transferência para bancária
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/bank_accounts/:bank_account_id/transfers
		 * @param integer $amount
		 * @param string $statement_descriptor
		 * @param string $description
	*/
	public function criarTransferenciaParaBancaria($amount, $statement_descriptor, $description) {
		$ret = $this->privateRequest(
			"POST",
			"/bank_accounts/$bank_account_id/transfers",
			[
				'amount'               => $amount,
				'statement_descriptor' => $statement_descriptor,
				'description'          => $description]);
		return $ret;
	}

	/*
		 * Listar contas bancárias por marketplace
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/bank_accounts
		 * @param string $marketplace_id
	*/
	public function listarContasBancariasPorMarketplace($marketplace_id) {
		$ret = $this->publicRequest(
			"GET",
			"/bank_accounts",
			[]);
		return $ret;
	}

	/*
		 * Associar conta bancaria com customer
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/bank_accounts
		 * @param string $customer
		 * @param string $token
	*/
	public function associarContaBancariaComCustomer($customer, $token) {
		$ret = $this->publicRequest(
			"POST",
			"/bank_accounts",
			[
				'customer' => $customer,
				'token'    => $token]);
		return $ret;
	}

	/*
		 * Recuperar detalhes de transferência
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/transfers/:transfer_id
		 * @param string $transfer_id
	*/
	public function recuperarDetalhesDeTransferencia($transfer_id) {
		$ret = $this->privateRequest(
			"GET",
			"/transfers/$transfer_id",
			[]);
		return $ret;
	}

	/*
		 * Listar transações associadas a transferência
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/transfers/:transfer_id/transactions
		 * @param string $transfer_id
	*/
	public function listarTransacoesAssociadasATransferencia($transfer_id) {
		$ret = $this->privateRequest(
			"GET",
			"/transfers/$transfer_id/transactions",
			[]);
		return $ret;
	}

	/*
		 * Listar transferência por marketplace
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/transfers
		 * @param string $marketplace_id
	*/
	public function listarTransferenciaPorMarketplace($marketplace_id) {
		$ret = $this->publicRequest(
			"GET",
			"/transfers",
			[]);
		return $ret;
	}

	/*
		 * Recuperar detalhes de plano de venda
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/plans/:plan_id
		 * @param string $plan_id
	*/
	public function recuperarDetalhesDePlanoDeVenda($plan_id) {
		$ret = $this->privateRequest(
			"GET",
			"/plans/$plan_id",
			[]);
		return $ret;
	}

	/*
		 * Remover plano de venda
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/plans/:plan_id
		 * @param string $plan_id
	*/
	public function removerPlanoDeVenda($plan_id) {
		$ret = $this->privateRequest(
			"DELETE",
			"/plans/$plan_id",
			[]);
		return $ret;
	}

	/*
		 * Criar plano de vendas
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/plans
	*/
	public function criarPlanoDeVendas() {
		$ret = $this->publicRequest(
			"POST",
			"/plans",
			[]);
		return $ret;
	}

	/*
		 * Listar plano de vendas por marketplace
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/plans
		 * @param string $marketplace_id
	*/
	public function listarPlanoDeVendasPorMarketplace($marketplace_id) {
		$ret = $this->publicRequest(
			"GET",
			"/plans",
			[]);
		return $ret;
	}

	/*
		 * Recuperar detalhes de assinatura de plano de venda
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/subscriptions/:subscription_id
		 * @param string $subscription_id
	*/
	public function recuperarDetalhesDeAssinaturaDePlanoDeVenda($subscription_id) {
		$ret = $this->privateRequest(
			"GET",
			"/subscriptions/$subscription_id",
			[]);
		return $ret;
	}

	/*
		 * Remover assinatura de plano de venda
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/subscriptions/:subscription_id
		 * @param string $subscription_id
	*/
	public function removerAssinaturaDePlanoDeVenda($subscription_id) {
		$ret = $this->privateRequest(
			"DELETE",
			"/subscriptions/$subscription_id",
			[]);
		return $ret;
	}

	/*
		 * Criar assinatura de plano de venda
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/subscriptions
	*/
	public function criarAssinaturaDePlanoDeVenda() {
		$ret = $this->publicRequest(
			"POST",
			"/subscriptions",
			[]);
		return $ret;
	}

	/*
		 * Listar assinaturas de plano de venda
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/subscriptions
		 * @param string $marketplace_id
	*/
	public function listarAssinaturasDePlanoDeVenda($marketplace_id) {
		$ret = $this->publicRequest(
			"GET",
			"/subscriptions",
			[]);
		return $ret;
	}

	/*
		 * Recuperar detalhes de recebível
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/receivables/:receivable_id
		 * @param string $receivable_id
	*/
	public function recuperarDetalhesDeRecebivel($receivable_id) {
		$ret = $this->privateRequest(
			"GET",
			"/receivables/$receivable_id",
			[]);
		return $ret;
	}

	/*
		 * Listar recebíveis por transação
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/transactions/:transaction_id/receivables
		 * @param string $transaction_id
	*/
	public function listarRecebiveisPorTransacao($transaction_id) {
		$ret = $this->privateRequest(
			"GET",
			"/transactions/$transaction_id/receivables",
			[]);
		return $ret;
	}

	/*
		 * Recuperar assinatura de plano de venda por comprador
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/buyers/:buyer_id/subscriptions
		 * @param string $buyer_id
	*/
	public function recuperarAssinaturaDePlanoDeVendaPorComprador($buyer_id) {
		$ret = $this->privateRequest(
			"GET",
			"/buyers/$buyer_id/subscriptions",
			[]);
		return $ret;
	}

	/*
		 * Criar transferência P2P
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v2/marketplaces/:marketplace_id/transfers/:owner/to/:receiver
		 * @param integer $amount
		 * @param string $description
	*/
	public function criarTransferenciaP2p($amount, $description) {
		$ret = $this->privateRequest(
			"POST",
			"/transfers/$owner/to/$receiver",
			[
				'amount'      => $amount,
				'description' => $description]);
		return $ret;
	}

}
?>