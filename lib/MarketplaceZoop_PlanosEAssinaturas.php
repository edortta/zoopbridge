<?php
require_once dirname(__FILE__) . "/MarketplaceZoop_MetodosDeCobranca.php";

class PlanosEAssinaturasZoop extends MetodosDeCobrancaZoop {

	/*
		 * Recupera um plano pelo identificador
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/marketplaces/:marketplace_id/plans/:plan_id
		 * @param string $plan_id
	*/
	public function recuperaUmPlanoPeloIdentificador($plan_id) {
		$ret = $this->privateRequest(
			"GET",
			"/plans/$plan_id",
			[]);
		return $ret;
	}

	/*
		 * Alterar plano pelo identificador
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/marketplaces/:marketplace_id/plans/:plan_id
	*/
	public function alterarPlanoPeloIdentificador() {
		$ret = $this->privateRequest(
			"PUT",
			"/plans/$plan_id",
			[]);
		return $ret;
	}

	/*
		 * Deletar um plano pelo identificador
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/marketplaces/:marketplace_id/plans/:plan_id
	*/
	public function deletarUmPlanoPeloIdentificador() {
		$ret = $this->privateRequest(
			"DELETE",
			"/plans/$plan_id",
			[]);
		return $ret;
	}

	/*
		 * Listar planos por marketplace
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/marketplaces/:marketplace_id/plans
		 * @param 20 $limit
		 * @param time-descending $sort
		 * @param int32 $offset
		 * @param number $date_range
		 * @param number $date_range_gt_
		 * @param number $date_range_gte_
		 * @param number $date_range_lt_
		 * @param number $date_range_lte_
	*/
	public function listarPlanosPorMarketplace($limit, $sort, $offset, $date_range, $date_range_gt_, $date_range_gte_, $date_range_lt_, $date_range_lte_) {
		$ret = $this->publicRequest(
			"GET",
			"/plans",
			[
				'limit'           => $limit,
				'sort'            => $sort,
				'offset'          => $offset,
				'date_range'      => $date_range,
				'date_range[gt]'  => $date_range_gt_,
				'date_range[gte]' => $date_range_gte_,
				'date_range[lt]'  => $date_range_lt_,
				'date_range[lte]' => $date_range_lte_]);
		return $ret;
	}

	/*
		 * Criar um plano
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/marketplaces/:marketplace_id/plans
		 * @param string $name
		 * @param string $frequency
		 * @param integer $interval
		 * @param Array $payment_methods
		 * @param string $currency
		 * @param integer $amount
		 * @param integer $setup_amount
		 * @param string $description
		 * @param integer $grace_period
		 * @param integer $tolerance_period
		 * @param integer $duration
	*/
	public function criarUmPlano($name, $frequency, $interval, $payment_methods, $currency, $amount, $setup_amount, $description, $grace_period, $tolerance_period, $duration) {
		$ret = $this->publicRequest(
			"POST",
			"/plans",
			[
				'name'             => $name,
				'frequency'        => $frequency,
				'interval'         => $interval,
				'payment_methods'  => $payment_methods,
				'currency'         => $currency,
				'amount'           => $amount,
				'setup_amount'     => $setup_amount,
				'description'      => $description,
				'grace_period'     => $grace_period,
				'tolerance_period' => $tolerance_period,
				'duration'         => $duration]);
		return $ret;
	}

	/*
		 * Recuperar os detalhes de uma assinatura pelo identificador
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/marketplaces/:marketplace_id/subscriptions/:subscription_id
		 * @param string $subscription_id
	*/
	public function recuperarOsDetalhesDeUmaAssinaturaPeloIdentificador($subscription_id) {
		$ret = $this->privateRequest(
			"GET",
			"/subscriptions/$subscription_id",
			[]);
		return $ret;
	}

	/*
		 * Alterar os detalhes de uma assinatura pelo identificador
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/marketplaces/:marketplace_id/subscriptions/:subscription_id
	*/
	public function alterarOsDetalhesDeUmaAssinaturaPeloIdentificador() {
		$ret = $this->privateRequest(
			"PUT",
			"/subscriptions/$subscription_id",
			[]);
		return $ret;
	}

	/*
		 * Remover uma assinatura pelo identificador
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/marketplaces/:marketplace_id/subscriptions/:subscription_id
	*/
	public function removerUmaAssinaturaPeloIdentificador() {
		$ret = $this->privateRequest(
			"DELETE",
			"/subscriptions/$subscription_id",
			[]);
		return $ret;
	}

	/*
		 * Recuperar faturas associadas a uma assinatura
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/marketplaces/:marketplace_id/subscriptions/:subscription_id/invoices
		 * @param 20 $limit
		 * @param int32 $offset
	*/
	public function recuperarFaturasAssociadasAUmaAssinatura($limit, $offset) {
		$ret = $this->privateRequest(
			"GET",
			"/subscriptions/$subscription_id/invoices",
			[
				'limit'  => $limit,
				'offset' => $offset]);
		return $ret;
	}

	/*
		 * Suspender uma assinatura pelo identificador
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/marketplaces/:marketplace_id/subscriptions/:subscription_id/suspend
	*/
	public function suspenderUmaAssinaturaPeloIdentificador() {
		$ret = $this->privateRequest(
			"POST",
			"/subscriptions/$subscription_id/suspend",
			[]);
		return $ret;
	}

	/*
		 * Reativa uma assinatura pelo identificador
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/marketplaces/:marketplace_id/subscriptions/:subscription_id/reactivate
	*/
	public function reativaUmaAssinaturaPeloIdentificador() {
		$ret = $this->privateRequest(
			"POST",
			"/subscriptions/$subscription_id/reactivate",
			[]);
		return $ret;
	}

	/*
		 * Recuperar todas as assinatura de um marketplace
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/marketplaces/:marketplace_id/subscriptions
		 * @param 20 $limit
		 * @param time-descending $sort
		 * @param int32 $offset
		 * @param number $date_range
		 * @param number $date_range_gt_
		 * @param number $date_range_gte_
		 * @param number $date_range_lt_
		 * @param number $date_range_lte_
	*/
	public function recuperarTodasAsAssinaturaDeUmMarketplace($limit, $sort, $offset, $date_range, $date_range_gt_, $date_range_gte_, $date_range_lt_, $date_range_lte_) {
		$ret = $this->publicRequest(
			"GET",
			"/subscriptions",
			[
				'limit'           => $limit,
				'sort'            => $sort,
				'offset'          => $offset,
				'date_range'      => $date_range,
				'date_range[gt]'  => $date_range_gt_,
				'date_range[gte]' => $date_range_gte_,
				'date_range[lt]'  => $date_range_lt_,
				'date_range[lte]' => $date_range_lte_]);
		return $ret;
	}

	/*
		 * Criar uma assinatura entre um comprador e um plano
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/marketplaces/:marketplace_id/subscriptions
		 * @param uuid $plan
		 * @param uuid $on_behalf_of
		 * @param uuid $customer
		 * @param string $payment_method
		 * @param date $due_date
		 * @param date $due_since_date
		 * @param date $expiration_date
		 * @param integer $amount
		 * @param string $currency
		 * @param integer $grace_period
		 * @param integer $tolerance_period
	*/
	public function criarUmaAssinaturaEntreUmCompradorEUmPlano($plan, $on_behalf_of, $customer, $payment_method, $due_date, $due_since_date, $expiration_date, $amount, $currency, $grace_period, $tolerance_period) {
		$ret = $this->publicRequest(
			"POST",
			"/subscriptions",
			[
				'plan'             => $plan,
				'on_behalf_of'     => $on_behalf_of,
				'customer'         => $customer,
				'payment_method'   => $payment_method,
				'due_date'         => $due_date,
				'due_since_date'   => $due_since_date,
				'expiration_date'  => $expiration_date,
				'amount'           => $amount,
				'currency'         => $currency,
				'grace_period'     => $grace_period,
				'tolerance_period' => $tolerance_period]);
		return $ret;
	}

	/*
		 * Recuperar os detalhes de uma fatura pelo identificador
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/marketplaces/:marketplace_id/invoices/:invoice_id
		 * @param string $invoice_id
	*/
	public function recuperarOsDetalhesDeUmaFaturaPeloIdentificador($invoice_id) {
		$ret = $this->privateRequest(
			"GET",
			"/invoices/$invoice_id",
			[]);
		return $ret;
	}

	/*
		 * Alterar detalhes de uma fatura pelo identificador
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/marketplaces/:marketplace_id/invoices/:invoice_id
	*/
	public function alterarDetalhesDeUmaFaturaPeloIdentificador() {
		$ret = $this->privateRequest(
			"PUT",
			"/invoices/$invoice_id",
			[]);
		return $ret;
	}

	/*
		 * Remover uma fatura pelo identificador
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/marketplaces/:marketplace_id/invoices/:invoice_id
	*/
	public function removerUmaFaturaPeloIdentificador() {
		$ret = $this->privateRequest(
			"DELETE",
			"/invoices/$invoice_id",
			[]);
		return $ret;
	}

	/*
		 * Aprovar fatura pendente
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/marketplaces/:marketplace_id/invoices/:invoice_id/approve
	*/
	public function aprovarFaturaPendente() {
		$ret = $this->privateRequest(
			"POST",
			"/invoices/$invoice_id/approve",
			[]);
		return $ret;
	}

	/*
		 * Estornar e reembolsar fatura
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/marketplaces/:marketplace_id/invoices/:invoice_id/void
	*/
	public function estornarEReembolsarFatura() {
		$ret = $this->privateRequest(
			"POST",
			"/invoices/$invoice_id/void",
			[]);
		return $ret;
	}

	/*
		 * Recuperar todas as faturas de um marketplace
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/marketplaces/:marketplace_id/invoices
		 * @param 20 $limit
		 * @param time-descending $sort
		 * @param int32 $offset
		 * @param number $date_range
		 * @param number $date_range_gt_
		 * @param number $date_range_gte_
		 * @param number $date_range_lt_
		 * @param number $date_range_lte_
	*/
	public function recuperarTodasAsFaturasDeUmMarketplace($limit, $sort, $offset, $date_range, $date_range_gt_, $date_range_gte_, $date_range_lt_, $date_range_lte_) {
		$ret = $this->publicRequest(
			"GET",
			"/invoices",
			[
				'limit'           => $limit,
				'sort'            => $sort,
				'offset'          => $offset,
				'date_range'      => $date_range,
				'date_range[gt]'  => $date_range_gt_,
				'date_range[gte]' => $date_range_gte_,
				'date_range[lt]'  => $date_range_lt_,
				'date_range[lte]' => $date_range_lte_]);
		return $ret;
	}

	/*
		 * Criar uma fatura avulsa
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/marketplaces/:marketplace_id/invoices
		 * @param uuid $subscription
		 * @param uuid $on_behalf_of
		 * @param uuid $customer
		 * @param text $description
		 * @param string $payment_method
		 * @param date $due_date
		 * @param date $expiration_date
		 * @param integer $amount
		 * @param string $currency
		 * @param integer $grace_period
		 * @param integer $tolerance_period
		 * @param integer $retries
		 * @param integer $max_retries
	*/
	public function criarUmaFaturaAvulsa($subscription, $on_behalf_of, $customer, $description, $payment_method, $due_date, $expiration_date, $amount, $currency, $grace_period, $tolerance_period, $retries, $max_retries) {
		$ret = $this->publicRequest(
			"POST",
			"/invoices",
			[
				'subscription'     => $subscription,
				'on_behalf_of'     => $on_behalf_of,
				'customer'         => $customer,
				'description'      => $description,
				'payment_method'   => $payment_method,
				'due_date'         => $due_date,
				'expiration_date'  => $expiration_date,
				'amount'           => $amount,
				'currency'         => $currency,
				'grace_period'     => $grace_period,
				'tolerance_period' => $tolerance_period,
				'retries'          => $retries,
				'max_retries'      => $max_retries]);
		return $ret;
	}

	/*
		 * Recuperar faturas associadas a um vendedor pelo identificador
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/marketplaces/:marketplace_id/sellers/:customer_id/invoices
		 * @param 20 $limit
		 * @param time-descending $sort
		 * @param int32 $offset
		 * @param number $date_range
		 * @param number $date_range_gt_
		 * @param number $date_range_gte_
		 * @param number $date_range_lt_
		 * @param number $date_range_lte_
	*/
	public function recuperarFaturasAssociadasAUmVendedorPeloIdentificador($limit, $sort, $offset, $date_range, $date_range_gt_, $date_range_gte_, $date_range_lt_, $date_range_lte_) {
		$ret = $this->privateRequest(
			"GET",
			"/sellers/$customer_id/invoices",
			[
				'limit'           => $limit,
				'sort'            => $sort,
				'offset'          => $offset,
				'date_range'      => $date_range,
				'date_range[gt]'  => $date_range_gt_,
				'date_range[gte]' => $date_range_gte_,
				'date_range[lt]'  => $date_range_lt_,
				'date_range[lte]' => $date_range_lte_]);
		return $ret;
	}

}
?>