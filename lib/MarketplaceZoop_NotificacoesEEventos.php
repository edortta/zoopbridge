<?php

require_once dirname(__FILE__) . "/MarketplaceZoop_ObjetosBase.php";

class NotificacoesEEventosZoop extends ObjetosBaseZoop {

	/*
		 * Recuperar detalhes de webhook
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/webhooks/:webhook_id
		 * @param string $webhook_id
	*/
	public function recuperarDetalhesDeWebhook($webhook_id) {
		$ret = $this->privateRequest(
			"GET",
			"/webhooks/$webhook_id",
			[]);
		return $ret;
	}

	/*
		 * Remover webhook
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/webhooks/:webhook_id
		 * @param string $webhook_id
	*/
	public function removerWebhook($webhook_id) {
		$ret = $this->privateRequest(
			"DELETE",
			"/webhooks/$webhook_id",
			[]);
		return $ret;
	}

	/*
		 * Cria webhook por marketplace
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/webhooks
	*/
	public function criaWebhookPorMarketplace() {
		$ret = $this->privateRequest(
			"POST",
			"/webhooks",
			[]);
		return $ret;
	}

	/*
		 * Listar webhooks por marketplace
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/webhooks
	*/
	public function listarWebhooksPorMarketplace() {
		$ret = $this->privateRequest(
			"GET",
			"/webhooks",
			[]);
		return $ret;
	}

	/*
		 * Listar eventos por marketplace
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/events
	*/
	public function listarEventosPorMarketplace() {
		$ret = $this->privateRequest(
			"GET",
			"/events",
			[]);
		return $ret;
	}

	/*
		 * Recuperar detalhes de evento pelo identificador
		 * @author Esteban D.Dortta
		 * @todo FALTA IMPLEMENTAR
		 * @access public
		 * @link {{baseUrl}}/v1/marketplaces/:marketplace_id/events/:event_id
		 * @param string $event_id
	*/
	public function recuperarDetalhesDeEventoPeloIdentificador($event_id) {
		$ret = $this->privateRequest(
			"GET",
			"/events/$event_id",
			[]);
		return $ret;
	}

}
?>