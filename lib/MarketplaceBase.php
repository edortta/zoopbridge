<?php

abstract class MarketplaceBase
{
    private $config;
    private $debugLevel;
    private $collectDebugTrace;

    private $error_messages = [
        100 => "out_dbg is not a resource",
        101 => "Unknown REST method '%s'",
        102 => "Payment type '%s' not recognized",
        103 => "SQL error when doing this: %s . Error: %s",
        200 => "OK",
        201 => "Created",
        304 => "Not Modified",
        400 => "Bad Request",
        401 => "Unauthorized",
        402 => "Request Failed",
        403 => "Forbidden",
        404 => "Not Found",
        500 => "Internal Server Error",
        502 => "Bad Gateway",
        600 => "Internal Client Error",
    ];

    public static function throwException($errorCode)
    {
        $args      = func_get_args();
        $errorCode = array_shift($args);
        if (isset($this)) {
            $msg = $this->error_messages[$errorCode];
            /* obvio que poderia ir na linha no fim do if. mas preciso depurar qual das duas está errada */
            $str = vsprintf($msg, $args);
        } else {
            $msg = "Error# $errorCode: " . str_repeat("%s ", count($args));
            /* obvio que poderia ir na linha no fim do if. mas preciso depurar qual das duas está errada */
            $str = vsprintf($msg, $args);
        }

        throw new Exception($str, 1);
    }

    private function _header_merge($a1, $a2, $unique = false)
    {
        if ($unique) {
            $auxRet = array();
            foreach ($a1 as $key => $value) {
                $value             = explode(":", $value);
                $auxRet[$value[0]] = trim($value[1]);
            }
            foreach ($a2 as $key => $value) {
                $value             = explode(":", $value);
                $auxRet[$value[0]] = trim($value[1]);
            }
            $ret = array();
            foreach ($auxRet as $key => $value) {
                $ret[] = "$key: $value";
            }
        } else {
            $ret = array_merge($a1, $a2);
        }
        return $ret;
    }

    public static function CPFCorreto($cpf)
    {
        $nulos = array("12345678909", "11111111111", "22222222222", "33333333333",
            "44444444444", "55555555555", "66666666666", "77777777777",
            "88888888888", "99999999999", "00000000000");
        $cpf = preg_replace("/[^0-9]+/", "", $cpf);

        if ((in_array($cpf, $nulos)) || (strlen($cpf) < 11)) {
            return false;
        } else {
            $acum = 0;
            for ($i = 0; $i < 9; $i++) {
                $acum += $cpf[$i] * (10 - $i);
            }

            $x    = $acum % 11;
            $acum = ($x > 1) ? (11 - $x) : 0;
            if ($acum != $cpf[9]) {
                return false;
            } else {
                $acum = 0;
                for ($i = 0; $i < 10; $i++) {
                    $acum += $cpf[$i] * (11 - $i);
                }

                $x    = $acum % 11;
                $acum = ($x > 1) ? (11 - $x) : 0;
                if ($acum != $cpf[10]) {
                    return false;
                } else {
                    return true;
                }
            }
        }

    }

    public static function CNPJCorreto($cnpj)
    {
        $cnpj = preg_replace("/[^0-9]+/", "", $cnpj);
        $d1   = 0;
        $d4   = 0;

        for ($n = 0; $n < strlen($cnpj) - 2; $n++) {
            if ($n < 4) {
                $f = 5 - $n;
            } else {
                $f = 13 - $n;
            }

            $d1 += $cnpj[$n] * $f;

            if ($n < 5) {
                $f = 6 - $n;
            } else {
                $f = 14 - $n;
            }

            $d4 += $cnpj[$n] * $f;
        }

        $r = ($d1 % 11);
        if ($r < 2) {
            $d1 = 0;
        } else {
            $d1 = 11 - $r;
        }

        $d4 += 2 * $d1;

        $r = ($d4 % 11);
        if ($r < 2) {
            $d2 = 0;
        } else {
            $d2 = 11 - $r;
        }

        $c = $d1 . $d2;
        $k = substr($cnpj, strlen($cnpj) - 2, 2);
        return ($c == $k);
    }

    public static function isValidEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    public static function isValidURL($url)
    {
        $path         = parse_url($url, PHP_URL_PATH);
        $encoded_path = array_map('urlencode', explode('/', $path));
        $url          = str_replace($path, implode('/', $encoded_path), $url);

        return filter_var($url, FILTER_VALIDATE_URL) ? true : false;

        // return filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_SCHEME_REQUIRED || FILTER_FLAG_HOST_REQUIRED);
    }

    public static function isValidDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    /**
     * Isto é muito grosseiro.
     * Como o costume (diferente do código civil) faz com que
     * os brasileiros coloquem o sobrenome do pai no final, podemos
     * - pelo menos - puxar o patronimico com confiança já que
     * ocupa a útlima posição
     * @param string $nomeCompleto
     * @return array
     */
    public function separarNomeDoSobrenome($nomeCompleto)
    {
        $nomeCompleto = trim($nomeCompleto);
        $n            = strrpos($nomeCompleto, " ");
        $ret          = array(
            trim(substr($nomeCompleto, 0, $n)),
            trim(substr($nomeCompleto, $n)),
        );
        return $ret;
    }

    public function buildReturn($http_code, $data, $debug, $error)
    {
        return array(
            'http_code' => $http_code,
            'data'      => $data,
            'debug'     => $debug,
            'error'     => $error);
    }

    public function triggerError($error)
    {
        return $this->buildReturn(600, null, null, $error);
    }

    public function getPublicURL()
    {
        return $this->config['public_api_url'];
    }

    public function getPrivateURL()
    {
        return sprintf($this->config['private_api_url'], $this->config['marketplace_id']);
    }

    private function prepareCurlChannel($sURL, $aParameters = array(), $aHeaders = array())
    {

        if ($this->debugLevel > 0) {
            echo "Calling $sURL\n";
            if ($this->collectDebugTrace) {
                $this->out_dbg_name = tempnam("/tmp", "zcall-");
                $this->out_dbg      = fopen($this->out_dbg_name, "w");
            }
        }

        $ch = curl_init();

        if ($this->collectDebugTrace) {
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            if (is_resource($this->out_dbg)) {
                curl_setopt($ch, CURLOPT_STDERR, $this->out_dbg);
            } else {
                $this->throwException(100);
            }

        } else {
            curl_setopt($ch, CURLOPT_VERBOSE, false);
            curl_setopt($ch, CURLOPT_HEADER, false);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_PROXY, @$this->config['proxy']);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);

        curl_setopt($ch, CURLOPT_FAILONERROR, false);

        curl_setopt($ch, CURLOPT_URL, "$sURL");

        $auxParameters = array_merge($this->config['headers'], $aHeaders);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $auxParameters);

        if ($this->debugLevel) {
            if (php_sapi_name() != 'cli') {
                echo "\n<PRE>";
            }

            echo "\nHEADERS:\n";
            foreach ($auxParameters as $key => $value) {
                echo "$value\n";
            }
            if (php_sapi_name() != 'cli') {
                echo "\n</PRE>";
            }
        }

        return $ch;
    }

    private function executeCurlChannel($ch)
    {

        if (function_exists("_log")) {
            $executionInfo = curl_getinfo($ch);
            _log("url:", $executionInfo['url']);
        }

        $server_output = trim(curl_exec($ch));

        $debug         = "";
        $executionInfo = curl_getinfo($ch);

        if ($this->collectDebugTrace) {
            $header_size = $executionInfo['header_size'];
            echo "\nHEADER_SIZE: $header_size\n";
            $headers_dbg   = substr($server_output, 0, $header_size);
            $server_output = substr($server_output, $header_size);

            if (is_resource($this->out_dbg)) {
                fclose($this->out_dbg);
                $debug = file_get_contents($this->out_dbg_name);
            }
        }

        $http_code  = $executionInfo['http_code'];
        $url        = $executionInfo['url'];
        $total_time = $executionInfo['total_time'];

        if (function_exists("_log")) {
            _log("http_code: $http_code");
            _log("total_time: $total_time");
        }

        if ($this->debugLevel) {
            if (php_sapi_name() != 'cli') {
                echo "\n<PRE>";
            }

            echo "\nHTTP Return Code: $http_code\n";
            echo "\nHeaders: " . $headers_dbg . "\n";
            echo "\nDebug: " . $debug . "\n";

            if (php_sapi_name() != 'cli') {
                echo "\n</PRE>";
            }

        }

        curl_close($ch);

        return array(
            'http_code'    => $http_code,
            'return'       => $server_output,
            'url'          => $url,
            'debug'        => $debug,
            'out_dbg_name' => @$this->out_dbg_name);
    }

    private function _POST($sURL, $aParameters = [], $aHeaders = [])
    {
        $params_string = json_encode($aParameters);
        $aux_headers   = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($params_string));
        $aux_headers = $this->_header_merge($aux_headers, $aHeaders);

        $ch = $this->prepareCurlChannel(
            $sURL,
            $aParameters,
            $aux_headers);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);

        if ($this->debugLevel) {
            echo "\nPARAMETERS:\n";
            echo json_encode($aParameters, JSON_PRETTY_PRINT) . "\n";
        }

        $tempRet        = $this->executeCurlChannel($ch);
        $tempRet['url'] = $sURL;

        return $tempRet;
    }

    private function _PUT($sURL, $aParameters = [], $aHeaders = [])
    {
        $params_string = json_encode($aParameters);
        $aux_headers   = array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($params_string));
        $aux_headers = $this->_header_merge($aux_headers, $aHeaders);
        $ch          = $this->prepareCurlChannel(
            $sURL,
            $aParameters,
            $aux_headers);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");

        if ($this->debugLevel) {
            echo "\nPARAMETERS:\n";
            echo json_encode($aParameters, JSON_PRETTY_PRINT) . "\n";
        }

        $tempRet        = $this->executeCurlChannel($ch);
        $tempRet['url'] = $sURL;

        return $tempRet;
    }

    private function _GET($sURL, $aParameters = [], $aHeaders = [])
    {
        $auxParameters = http_build_query($aParameters);
        if ($auxParameters > '') {
            $sURL .= "?$auxParameters";
        }
        $aux_headers = array();
        $aux_headers = $this->_header_merge($aux_headers, $aHeaders);

        $ch = $this->prepareCurlChannel($sURL);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

        $tempRet        = $this->executeCurlChannel($ch);
        $tempRet['url'] = $sURL;

        return $tempRet;
    }

    private function _DELETE($sURL, $aParameters = [], $aHeaders = [])
    {
        $ch = $this->prepareCurlChannel($sURL, $aParameters);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");

        $tempRet        = $this->executeCurlChannel($ch);
        $tempRet['url'] = $sURL;

        return $tempRet;
    }

    private function _request($method, $url, $aParameters = [], $aHeaders = [])
    {
        switch ($method) {
            case "POST":
                return $this->_POST($url, $aParameters, $aHeaders);
                break;

            case "GET":
                return $this->_GET($url, $aParameters, $aHeaders);
                break;

            case "PUT":
                return $this->_PUT($url, $aParameters, $aHeaders);
                break;

            case "DELETE":
                return $this->_DELETE($url, $aParameters, $aHeaders);
                break;

            default:
                $this->throwException(101, $method);
                break;
        }
    }

    public function publicRequest($method, $cURL, $aParameters = [], $aHeaders = [])
    {
        $url = $this->getPublicURL() . $cURL;
        return $this->_request($method, $url, $aParameters, $aHeaders);
    }

    public function privateRequest($method, $cURL, $aParameters = [], $aHeaders = [])
    {
        $url = $this->getPrivateURL() . $cURL;
        return $this->_request($method, $url, $aParameters, $aHeaders);
    }

    public function showDebug($dbg)
    {
        if ($this->collectDebugTrace) {
            if (php_sapi_name() != 'cli') {
                echo "\n<PRE>";
            }

            echo "\n+---[DEPURACAO]-----------\n";
            echo "$dbg";
            echo "\n+---[/DEPURACAO]----------\n";

            if (php_sapi_name() != 'cli') {
                echo "\n</PRE>";
            }

        }
    }

    public function configure($JConfig, $cleanup = true)
    {
        /****************
        Recebe um JSON e um booleano
        O JConfig é um JSON para permitir uma maior flexibilidade em integrações futuras.
        Nele são esperados pelo menos os seguintes campos:
        url_api
        versao_api
        token
        marketplace_id
        Mas também podem ser usados os seguintes:
        usuario
        senha
        chave
        chave_api
        E outros que as implementações venham a precisar.

        O cleanup é um booleano que -estando em true- elimina a configuração anterior
        substituindo-a pela mesma. Se ele estiver em false, o JConfig será misturado
        à configuração existente podendo assim modificar um ou outro parâmetro da configuração
         ****************/
        if ($cleanup) {
            $this->config = json_decode($JConfig, true);
        } else {
            $auxConfig = json_decode($JConfig, true);
            foreach ($auxConfig as $key => $value) {
                $this->config[$key] = $value;
            }
        }

        if (substr($this->config['url_api'], strlen($this->config['url_api']) - 1, 1) == '/') {
            $this->config['url_api'] = substr($this->config['url_api'], 0, strlen($this->config['url_api']) - 1);
        }

        $this->config['public_api_url']  = $this->config['url_api'] . "/" . $this->config['versao_api'];
        $this->config['private_api_url'] = $this->config['url_api'] . "/" . $this->config['versao_api'] . "/marketplaces/%s";
        $this->config['headers']         = array(
            'Accept: application/json',
            'Authorization: Basic ' . base64_encode($this->config['token'] . ":"),
        );

        $this->debugLevel        = isset($this->config['debugLevel']) ? $this->config['debugLevel'] : 0;
        $this->collectDebugTrace = ($this->debugLevel > 1);
    }

}
