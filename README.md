# ZoopBridge

**Este é um conjunto de classes em PHP para integrar seu projeto com a Zoop**

- Esta iniciativa não é - de forma alguma - realizada ou autenticada pela Zoop.
- É a nossa ferramenta de implementação disponibilizada à comunidade de programadores.

## Herança

As classes formam apenas uma classe no final, mas - seguindo o POSTMAN da zoop - propomos a seguinte linha de herança:

```
MarketplaceZoop
  ==> PlanosEAssinaturasZoop
    ==> MetodosDeCobrancaZoop
      ==> UsuariosEApiKeysZoop
        ==> NotificacoesEEventosZoop
          ==> ObjetosBaseZoop
            ==> MarketplaceBase
```

## Exemplo simples

Digamos que temos o CPF do vendedor (175.582.650-88). Usando a classe MarketplaceZoop podemos construir uma consulta simples:

```php
<?php
/**
 * Este exemplo reside na pasta 'exemplos'
 **/
require_once "../lib/MarketplaceZoop.php";

/**
 * A configuração do proxy é opcional mas a incluimos à guisa de exemplo
 * Caso não use proxy, é apenas eliminar a linha
 **/
$zoop_config = [
    'proxy'          => 'http://192.168.56.1:3128',
    'url_api'        => 'https://api.zoop.ws/',
    'versao_api'     => 'v1',
    'token'          => 'zpk_test_EzCkzFFKibGQU6HFq7EYVuxI',
    'marketplace_id' => '3249465a7753536b62545a6a684b0000',
    'debugLevel'     => 0,
];

$marketplaceZoop = new MarketplaceZoop();
$marketplaceZoop->configure(json_encode($zoop_config));

$dados = $marketplaceZoop->recuperarVendedorPorDocumento("175.582.650-88");

if (200 == $dados['http_code']) {
  print_r($dados['return']);  
}
?>
```



Todos os exemplos foram idealizados para serem rodados na linha de comandos.

Todas as chamadas à classe MarketplaceZoop devolvem um vetor associativo com os seguintes campos:

| Nome      | Descrição                                                    |
| --------- | ------------------------------------------------------------ |
| http_code | Código http do resultado da consulta à API da Zoop. Erros iguais ou superiores a 600 pertencem à classe MarketplaceZoop |
| return    | JSON contendo o retorno enviado pela Zoop                    |
| url       | URL usada na consulta. Serve para poder analisar o que está de fato sendo consumido na API da Zoop |
| debug     | Depuração devolvida pelo CURL quando debugLevel está acima de 0 |

## Analisando a consulta

Além das variáveis de retorno, muita informação pode ser obtida justo na hora em que a API da Zoop é consumida. Para isso crie uma função chamada *_log()* como no exemplo a seguir:

```php
<?php
require_once "../lib/MarketplaceZoop.php";
  
function _log() {
  $args = func_get_args();
  $logEntry = date("Ymd H:i:s ");
  foreach ($args as $value) {
    $logEntry .= "$value ";
  }
  echo "$logEntry\n";
}

$zoop_config = [
    'url_api'        => 'https://api.zoop.ws/',
    'versao_api'     => 'v1',
    'token'          => 'zpk_test_EzCkzFFKibGQU6HFq7EYVuxI',
    'marketplace_id' => '3249465a7753536b62545a6a684b0000',
    'debugLevel'     => 0,
];

$marketplaceZoop = new MarketplaceZoop();
$marketplaceZoop->configure(json_encode($zoop_config));

/* EXEMPLO: puxar a lista dos MMC a partir do registro nro 102 */
print_r($marketplaceZoop->listarMccsMerchantCategoryCodes(array('offset' => 102)));

?>
```



