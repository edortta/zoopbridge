<?php
require_once "../lib/MarketplaceZoop.php";

function myecho($aInfo)
{
    echo "\n<pre>\n====[ START ]" . str_repeat("=", 120) . "\n";
    foreach ($aInfo as $key => $value) {
        echo "[ $key ] => " . str_replace("\n", "\n\t", $value) . "\n";
    }
    echo "\n====[ END ]" . str_repeat("=", 120) . "\n\n\n</pre>\n";
}

$zoop_config = [
    'proxy'          => 'http://192.168.56.1:3128',
    'url_api'        => 'https://api.zoop.ws/',
    'versao_api'     => 'v1',
    'token'          => 'zpk_test_EzCkzFFKibGQU6HFq7EYVuxI',
    'marketplace_id' => '3249465a7753536b62545a6a684b0000',
    'debugLevel'     => 0,
];

$marketplaceZoop = new MarketplaceZoop();
$marketplaceZoop->configure(json_encode($zoop_config));

/* EXEMPLO Recuperar dados da pessoa pelo documento */
myecho($marketplaceZoop->recuperarVendedorPorDocumento("175.582.650-88"));
