<?php

/**
 * Carregamos a livraria MarketplaceZoop
 * Este script está na pasta /exemplos ao passo que
 * ela está na /lib
 **/
require_once "../lib/MarketplaceZoop.php";

/**
 * Configuração do modo de operação
 * O proxy é só uma sugestão mas pode ser eliminado
 **/
$zoop_config = [
    'proxy'          => 'http://192.168.56.1:3128',
    'url_api'        => 'https://api.zoop.ws/',
    'versao_api'     => 'v1',
    'token'          => 'zpk_test_EzCkzFFKibGQU6HFq7EYVuxI',
    'marketplace_id' => '3249465a7753536b62545a6a684b0000',
    'debugLevel'     => 0,
];

// Inicializo uma instância do MarketplaceZoop
$marketplaceZoop = new MarketplaceZoop();
// Configuro a mesma
$marketplaceZoop->configure(json_encode($zoop_config));

/**
 * Este script espera um parâmetro contendo o ID do cliente
 * Caso esse parâmetro não esteja presente, ele assume um ID conhecido
 * apenas para mostrar a funcionalidade
 **/
$idCliente = ($argc > 1 ? $argv[1] : "424ac33553004251bb62aa69bf15538f");

$comprador = $marketplaceZoop->recuperarDetalhesComprador($idCliente);
echo "Comprador:\n";
print_r($comprador);

$vendedor = $marketplaceZoop->recuperarDetalhesVendedor($idCliente);
echo "Vendedor:\n";
print_r($comprador);
