<?php
require_once "../lib/MarketplaceZoop.php";

function myecho($aInfo)
{
    echo "\n<pre>\n====[ START ]" . str_repeat("=", 120) . "\n";
    foreach ($aInfo as $key => $value) {
        echo "[ $key ] => " . str_replace("\n", "\n\t", $value) . "\n";
    }
    echo "\n====[ END ]" . str_repeat("=", 120) . "\n\n\n</pre>\n";
}

/* EXEMPLO: Definir o nivel de depuração para 2 */
$marketplaceZoop->configure('{"debugLevel": "0"}', false);

/* EXEMPLO: puxar a lista dos MMC */
myecho($marketplaceZoop->listarMCC(array('offset' => 102)));
//exit;

/* EXEMPLO: cadastrar um pizzaiolo */

myecho($marketplaceZoop->cadastrarVendedorCPF(
    "175.582.650-88",
    "Daniel", "Dos Testes",
    "Mr.Pizza",
    "Pizzaiolo amador",
    "1971-04-19",
    "Rua José Maria das Penas",
    "851",
    "Depois das botas endiabradas",
    "1902070",
    "Jardim das Flores",
    "Vereador Zé Mané",
    "SP",
    "BR",
    "pumba@jungla.com",
    "+5511993420449",
    "https://www.yahoo.com.ar",
    "99"
));

/* EXEMPLO Recuperar dados da pessoa pelo documento */
myecho($marketplaceZoop->recuperarVendedorPorDocumento("175.582.650-88"));

/* EXEMPLO de modificação dos dados cadastrais */
/* No cadastro (ou no recuperarVendedorPorDocumento()) o id devolvido foi "5b0bad5f3635499a8edd84a6f027d155" */

myecho($marketplaceZoop->modificarVendedorCPF(
    "5b0bad5f3635499a8edd84a6f027d155",
    "175.582.650-88",
    "Daniel", "Dos Testes",
    "Mr.Pizza",
    "Pizzaiolo amador",
    "1960-12-15",
    "Rua João Jacinto Câmara",
    "851",
    null,
    "79113090",
    "Jardim Panamá",
    "CAMPO GRANDE",
    "MS",
    "BR",
    "pumba@jungla.com.br",
    "+5511993420449",
    "https://www.yahoo.com.ar",
    "99"));

/* EXEMPLO: mostrar os documentos da Herminia Senger */
/* ffdde264f85c47b0880e61f4c2114e3b */
myecho($marketplaceZoop->listarDocumentosVendedor("ffdde264f85c47b0880e61f4c2114e3b"));

/* EXEMPLO: mostrar os dados da Herminia Senger */
myecho($marketplaceZoop->recuperarDetalhesVendedor("ffdde264f85c47b0880e61f4c2114e3b"));

/* EXEMPLO: mostrar os dados o pizzaiolo */
myecho($marketplaceZoop->recuperarDetalhesVendedor("5b0bad5f3635499a8edd84a6f027d155"));

/* EXEMPLO: listar vendedores */
myecho($marketplaceZoop->listarVendedores(array("offset" => "100")));

/* EXEMPLO: listar compradores */
myecho($marketplaceZoop->listarCompradores(array("offset" => "120")));
