<?php
require_once "../lib/MarketplaceZoop.php";

function myecho($aInfo)
{
    echo "\n<pre>\n====[ START ]" . str_repeat("=", 120) . "\n";
    foreach ($aInfo as $key => $value) {
        echo "[ $key ] => " . str_replace("\n", "\n\t", $value) . "\n";
    }
    echo "\n====[ END ]" . str_repeat("=", 120) . "\n\n\n</pre>\n";
}

$marketplaceZoop->configure('{"debugLevel": "0"}', false);

/*
Este exemplo parte do suposto que você já criou um usuário usando o outro script.
O nosso CPF de testes é 175.582.650-88
 */

$cpfVendedor = "175.582.650-88";
$idComprador = "3c7378501aed4f8497b1891af9b84a71";

$infoVendedor = $marketplaceZoop->recuperarVendedorPorDocumento($cpfVendedor);

if ($infoVendedor['http_code'] == 200) {
    $return      = json_decode($infoVendedor['return'], true);
    $idVendedor  = $return['id'];
    $hoje        = strtotime(date("Y-m-d"));
    $vencimento  = date("Y-m-d", strtotime("+1 month", $hoje));
    $vencimento2 = date("Y-m-d", strtotime("+2 month", $hoje));

    $infoComprador = $marketplaceZoop->recuperarDetalhesComprador($idComprador);
    if ($infoComprador['http_code'] == 200) {
        $infoComprador = json_decode($infoComprador['return'], true);

        echo "Vendedor\nCPF:\t$cpfVendedor\nID:\t$idVendedor\nVenc:\t$vencimento\nComprador\nCPF:\t" . $infoComprador['taxpayer_id'] . "\nID:$idComprador\n";

        $marketplaceZoop->configure('{"debugLevel": "2"}', false);

        $boleto = $marketplaceZoop->criarCobrancaBoleto(
            $idComprador, /* pagador */
            $idVendedor, /* beneficiario */
            md5(date('U')), /* referencia no conta corrente */
            10050, /* valor em numero inteiro */
            "Teste de emissão de boleto",
            "Este é um teste de emissão de boleto",
            "https://www.inovacaosistemas.com.br/img/InovacaoSistemas-Icone-transparente-2019.png", /* url da logo */
            $vencimento,
            $vencimento,
            3,
            3,
            $vencimento2,
            $hoje,
            8500
        );

        print_r($boleto);
    }

}
