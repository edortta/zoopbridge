<?php

require_once "../../../inc/conecta.php";

require_once "../MarketplaceZoop.php";

/**
 *
 */
class MeuMarketplaceZoop extends MarketplaceZoop
{

    /*
     * Incorpora cliente existente na ZOOP dentro da tabela clientes
     * em uma 'agência' específica.
     * Ele recebe dados cedidos por /buyers, /sellers
     * @author Esteban D.Dortta
     * @todo
     * @access public
     * @param string $transaction_id
     * @param string $id
     */
    public function incorporarClienteZoop($agencia, $infoCliente)
    {
        global $conecta;

        if (is_array($infoCliente)) {
            reset($infoCliente);
            $isSequential = key($infoCliente) === 0;
        } else {
            $isSequential = false;
        }

        if ($isSequential) {
            for ($i = 0; $i < count($infoCliente); $i++) {
                $this->incorporarClienteZoop($agencia, $infoCliente[$i]);
            }
        } else {
            $infoCliente['_ativo']          = 1;
            $infoCliente['_agencia']        = $agencia;
            $infoCliente['_tipo_documento'] = 'cpf';
            $equivalencia                   = array(
                "agencia"                            => "_agencia",
                "ativo"                              => "_ativo",
                "tipo_documento"                     => "_tipo_documento",
                "conta"                              => "current_balance",
                "conta_investimento"                 => "account_balance",
                "nome"                               => "description",
                "zoop_id"                            => "id",
                "zoop_statement_descriptor"          => "statement_descriptor",
                "zoop_first_name"                    => "first_name",
                "zoop_last_name"                     => "last_name",
                "zoop_mcc"                           => "mcc",
                "zoop_is_seller"                     => "resource_seller",
                "zoop_is_buyer"                      => "resource_buyer",
                "zoop_website"                       => "website",
                "zoop_facebook"                      => "facebook",
                "zoop_twitter"                       => "twitter",
                "zoop_show_profile_online"           => "show_profile_online",
                "zoop_is_mobile"                     => "is_mobile",
                "zoop_decline_on_fail_security_code" => "decline_on_fail_security_code",
                "zoop_decline_on_fail_zipcode"       => "decline_on_fail_zipcode",
                "zoop_delinquent"                    => "delinquent",
                "zoop_payment_methods"               => "payment_methods",
                "zoop_merchant_code"                 => "merchant_code",
                "zoop_terminal_code"                 => "terminal_code",
                "zoop_uri"                           => "uri",
                "zoop_marketplace_id"                => "marketplace_id",
                "zoop_created_at"                    => "created_at",
                "zoop_updated_at"                    => "updated_at",
                "cpf"                                => "taxpayer_id",
                "data_nascimento"                    => "birthdate",
                "email"                              => "email",
                "telefone"                           => "phone_number",
                "cep"                                => "address_postal_code",
                "endereco"                           => "address_line1",
                "numero"                             => "address_line2",
                "complemento"                        => "address_line3",
                "bairro"                             => "address_neighborhood",
                "cidade"                             => "address_city",
                "estado"                             => "address_state",
            );
            if ((!isset($infoCliente['description'])) || (trim($infoCliente['description']) == '')) {
                $infoCliente['description'] = $infoCliente['first_name'] . ' ' . $infoCliente['last_name'];
            }
            foreach ($infoCliente['address'] as $key => $value) {
                $infoCliente['address_' . $key] = $value;
            }
            $infoCliente['resource_buyer']  = $infoCliente['resource'] == 'buyer' ? 1 : 0;
            $infoCliente['resource_seller'] = $infoCliente['resource'] == 'seller' ? 1 : 0;

            $sqlFields = "";
            $sqlValues = "";

            $zoop_id = $infoCliente['id'];
            $cpf     = $infoCliente['taxpayer_id'];

            $q = mysqli_query($conecta, "select id from clientes where (zoop_id='$zoop_id') or (agencia=$agencia and cpf='$cpf')");
            if ($q) {
                $d  = mysqli_fetch_assoc($q);
                $id = $d['id'];
                if ($id > '') {
                    $sqlFields = 'id';
                    $sqlValues = "$id";
                }
                mysqli_free_result($q);
            }

            foreach ($equivalencia as $key => $value) {
                if ($sqlFields > '') {
                    $sqlFields .= ", ";
                    $sqlValues .= ", ";
                }
                $sqlFields .= $key;
                $sqlValues .= "'" . addslashes(@$infoCliente[$value]) . "'";
            }

            echo "id: " . $infoCliente['id'] . " $id             \r";

            $sql = "replace into clientes ($sqlFields) values ($sqlValues)";

            $q = mysqli_query($conecta, $sql);
            if (!$q) {
                $this->throwException(103, $sql);
            }
        }
    }

}

function myecho($aInfo)
{
    echo "\n<pre>\n====[ START ]" . str_repeat("=", 120) . "\n";
    foreach ($aInfo as $key => $value) {
        echo "[ $key ] => " . str_replace("\n", "\n\t", $value) . "\n";
    }
    echo "\n====[ END ]" . str_repeat("=", 120) . "\n\n\n</pre>\n";
}

$zoop_config = [
    'proxy'          => 'http://192.168.56.1:3128',
    'url_api'        => 'https://api.zoop.ws/',
    'versao_api'     => 'v1',
    'token'          => 'zpk_test_EzCkzFFKibGQU6HFq7EYVuxI',
    'marketplace_id' => '3249465a7753536b62545a6a684b0000',
    'debugLevel'     => 0,
];

$marketplaceZoop = new MeuMarketplaceZoop();
$marketplaceZoop->configure(json_encode($zoop_config));

$offset = 0;
$total  = -1;
do {
    echo "offset: $offset                                                \n";
    $dados = $marketplaceZoop->listarCompradores(array('offset' => $offset));
    if ($dados['http_code'] == 200) {
        $return = json_decode($dados['return'], true);
        $total  = $return['total'];
        $offset += $return['limit'];
        for ($i = 0; $i < count($return['items']); $i++) {
            if (trim($return['items'][$i]['first_name'] . $return['items'][$i]['first_name']) > '') {
                $marketplaceZoop->incorporarClienteZoop(6, $return['items'][$i]);
            }
        }
    } else {
        $offset = $total + 1;
    }
} while ($offset < $total);
