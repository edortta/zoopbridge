<?php
require_once "lib/MarketplaceZoop.php";

function myecho($aInfo)
{
    echo "\n<pre>\n====[ START ]" . str_repeat("=", 120) . "\n";
    foreach ($aInfo as $key => $value) {
        echo "[ $key ] => " . str_replace("\n", "\n\t", $value) . "\n";
    }
    echo "\n====[ END ]" . str_repeat("=", 120) . "\n\n\n</pre>\n";
}

$zoop_config = [
    'proxy'          => 'http://192.168.56.1:3128',
    'url_api'        => 'https://api.zoop.ws/',
    'versao_api'     => 'v1',
    'token'          => 'zpk_test_EzCkzFFKibGQU6HFq7EYVuxI',
    'marketplace_id' => '3249465a7753536b62545a6a684b0000',
    'debugLevel'     => 0,
];

$marketplaceZoop = new MarketplaceZoop();
$marketplaceZoop->configure(json_encode($zoop_config));

/* EXEMPLO: Definir o nivel de depuração para 2 */
$marketplaceZoop->configure('{"debugLevel": "2"}', false);

echo "MarketplaceZoop\n";
$parents   = class_parents($marketplaceZoop);
$n         = 1;
$first_key = '';
foreach ($parents as $key => $value) {
    if ($n == 1) {
        $first_key = $key;
    }

    echo str_repeat("  ", $n++) . "==> $key\n";
}

echo "\nMétodos:\n";

$methods = get_class_methods($first_key);
echo "  $key\n";
asort($methods);
foreach ($methods as $name) {
    echo "    $name\n";
}
echo "\n";
